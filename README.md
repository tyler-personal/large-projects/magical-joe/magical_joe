# Magical Joe
Originally a Discord Bot, but now a generalized command handler with the ability to store/analyze data.

Has a Discord/Telegram/Web frontend, with domain logic that is abstracted entirely from the presentation and data dependencies (APIs/DB/JSON). 

The domain layer gets injected wtih the proper dependencies manually (not using a DI framework) through a container I built, which will differ based on which frontend is being presented.

As of right now, I just did a major refactor and didn't carry over the Telegram frontend (there were bugs, want to find a better more concise framework anyway), 
so only Discord/Web are functional right now.