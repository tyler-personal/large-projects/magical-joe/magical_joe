package presentation

import com.vdurmont.emoji.EmojiParser
import domain.views.*
import domain.views.bots.*
import domain.views.bots.plugins.*
import domain.views.channels.Channel
import domain.views.servers.Server
import domain.views.channels.TextChannel
import domain.views.events.*
import domain.views.users.*
import lib.*
import log
import org.javacord.api.DiscordApi
import org.javacord.api.DiscordApiBuilder
import presentation.discord.*
import presentation.discord.channels.DiscordChannel
import presentation.discord.channels.DiscordTextChannel
import presentation.discord.events.*
import java.util.concurrent.CompletableFuture

class DiscordBot : SimpleBot, VoicePlugin, WebBot, VotingBot, QuoteBot {
    private val botToken = "NDc5Nzk5ODYyNzU0Mjc5NDI1.DlegSw.S9Byv4MOgofCjKHh-8dENrGUKLw"
    private val api: DiscordApi = DiscordApiBuilder().setToken(botToken).login().join()

    init {
        api.addMessageCreateListener { event ->
            val li = listOf("potato")
            li.forEach(event.message::addReaction)
        }
    }
    override fun commandListener(command: String, block: CommandEvent.() -> Unit) {
        api.addMessageCreateListener { event ->
            block(DiscordCommandEvent(event, command))
        }
    }

    override fun getChannelByIdForRouting(id: String): Channel? =
        api.getServerChannelById(id).orElse(null)?.let(::DiscordChannel)

    override fun getTextChannelById(id: String): TextChannel? =
        getDiscordTextChannel(id)

    override fun getMessageById(id: String, channel: TextChannel): Message? = tryOrNull {
        DiscordMessage(api.getMessageById(id, getDiscordTextChannel(channel.id)!!.textChannel).get())
    }
    override fun getUserById(id: String): VoiceUser? = tryOrNull {
        DiscordUser(api.getUserById(id).get())
    }

    override fun getUserByName(name: String): User? = tryOrNull {
        DiscordUser(api.getCachedUserByDiscriminatedName(name).get())
    }

    override fun imageCommandListener(command: String, block: ImageCommandEvent.() -> Unit) {
        api.addMessageCreateListener { event ->
            val imageBytes = event.message.attachments.firstOrNull { it.isImage }?.downloadAsByteArray()
            val msg = event.message.content

            fun execute(bytes: ByteArray) = block(DiscordImageCommandEvent(event, command, bytes))

            when {
                imageBytes != null -> imageBytes.whenComplete { result, _ -> execute(result) }
                msg.isHTTP && msg.isIMG -> execute(java.net.URL(msg).readBytes())
            }
        }
    }

    override val servers get() = api.servers.map(::DiscordServer)

    override fun getServerByIdForRouting(id: String): Server? = servers.find { it.id == id }

    private fun getDiscordTextChannel(id: String) =
        api.getTextChannelById(id).orElse(null)?.let(::DiscordTextChannel)
}
