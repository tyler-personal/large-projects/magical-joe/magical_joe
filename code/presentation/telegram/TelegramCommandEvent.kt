package presentation.telegram

import domain.entities.enums.UserStatus
import domain.views.SimpleMessage
import domain.views.channels.*
import domain.views.events.CommandEvent
import domain.views.events.MessageEvent
import domain.views.servers.Server
import domain.views.users.User
import me.ivmg.telegram.Bot
import me.ivmg.telegram.entities.*
import io.github.vjames19.futures.jdk8.Future

open class TelegramCommandEvent(private val bot: Bot, update: Update, override val command: String) : CommandEvent {
    private val msg = update.message!!
    private val chat = msg.chat

    override val id: String = "${msg.messageId}"
    override val message = msg.text!!

    override val user: User = object : User {
        private val user = msg.from!!
        override var name = user.firstName
        override val id = "${user.id}"
        override val status = UserStatus.ACTIVE

        override val isBot: Boolean = user.isBot

        override val servers: List<Server> = listOf()
        override val textChannels: List<TextChannel> = listOf()

        override fun sendMessage(content: String) {
            bot.sendMessage(user.id, content)
        }

        override fun privateMessage(): MessageEvent {
            return Future {
                var result: MessageEvent? = null

                TelegramBot.listeners.add(TelegramBot.Listener("") {
                    result = this
                })
                while (result == null)
                    Thread.sleep(50)
                result!!
            }.get()
        }

        override fun getNickname(server: Server): String? {
            // bot.getChat(server.id)
            return null
        }
    }
    override val server: Server? = null
    override val channel: TextChannel? = null

    override val mentionedUsers: List<User> = listOf()

    override fun deleteMessage() {
        bot.deleteMessage(chat.id, msg.messageId)
    }

    override fun displayPictureByUrl(url: String) {
        bot.sendPhoto(chat.id, url)
    }

    override fun reply(content: String) = privateReply(content)

    override fun replyWithCode(content: String, language: String) = privateReply("```$content```")

    override fun replyWithNamedLink(title: String, url: String) { privateReply("[$title]($url)") }

    private fun privateReply(content: String): SimpleMessage? {
        val msg = bot.sendMessage(chat.id, content, ParseMode.MARKDOWN).first?.body()?.result ?: return null

        return object : SimpleMessage {
            override val id = "${msg.messageId}"
            override val message = msg.text ?: ""
            override fun appendMessage(x: String) {
                bot.editMessageText(chat.id, msg.messageId, text = "$message$x")
            }
        }
    }

    override val String.hideURL get() = "[], ($this)"
    override val String.italics get() = "__${this}__"
    override val String.bold get() = "**${this}**"
    override val String.codeSnip get() = "`${this}`"
}