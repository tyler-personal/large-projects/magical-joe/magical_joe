package presentation.telegram

import domain.views.bots.SimpleBot
import domain.views.events.*
import me.ivmg.telegram.dispatch
import me.ivmg.telegram.dispatcher.document
import me.ivmg.telegram.dispatcher.handlers.media.PhotosHandler
import me.ivmg.telegram.dispatcher.photos
import me.ivmg.telegram.dispatcher.text
import me.ivmg.telegram.extensions.filters.Filter
import java.io.File
import me.ivmg.telegram.bot as telegramBot

class TelegramBot : SimpleBot {
    data class Listener(val command: String, val block: CommandEvent.() -> Unit)
    data class ImageListener(val command: String, val block: ImageCommandEvent.() -> Unit)

    companion object {
        val listeners = mutableListOf<Listener>()
        val imageListeners = mutableListOf<ImageListener>()
    }

    private var lastUpdate = -1L
    private var lastImageUpdate = -1L

    init {
        val bot = telegramBot {
            token = "674682652:AAF4olgSH-VHoZow31RBIOkRh_e8izNoBkI"

            dispatch {
                this.photos { bot, update, list ->
                    if (update.updateId != lastImageUpdate) {
                        val image = bot.getFile(list[0].fileId).first?.body()?.result
                        if (image?.filePath != null) {
                            // val bytes = File(image.filePath!!).readBytes()
                            //imageListeners.forEach { (command, block) ->
                                // block(TelegramImageCommandEvent(bot, update, command, bytes))
                            //}
                        }
                        bot.getFile(list[0].fileId)
                    }
                }
                text { bot, update ->
                    if (update.updateId != lastUpdate) {
                        listeners.forEach { (command, block) ->
                            block(TelegramCommandEvent(bot, update, command))
                        }
                    }

                    lastUpdate = update.updateId
                }
            }
        }
        bot.startPolling()
    }

    override fun commandListener(command: String, block: CommandEvent.() -> Unit) {
        listeners.add(Listener(command, block))
    }

    override fun imageCommandListener(command: String, block: ImageCommandEvent.() -> Unit) {
        imageListeners.add(ImageListener(command, block))
    }
}