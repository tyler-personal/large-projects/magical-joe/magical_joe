package presentation.telegram

import domain.views.events.ImageCommandEvent
import me.ivmg.telegram.Bot
import me.ivmg.telegram.entities.*

class TelegramImageCommandEvent(
        bot: Bot, update: Update, command: String, override val messageAsBytes: ByteArray
) : TelegramCommandEvent(bot, update, command), ImageCommandEvent