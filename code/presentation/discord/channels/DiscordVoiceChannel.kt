package presentation.discord.channels

import domain.views.channels.VoiceChannel
import presentation.discord.DiscordUser
import org.javacord.api.entity.channel.ServerVoiceChannel as JVoiceChannel

class DiscordVoiceChannel(channel: JVoiceChannel) : DiscordChannel(channel.asServerChannel().get()), VoiceChannel {
    override val connectedUsers = channel.connectedUsers.map(::DiscordUser)
}