package presentation.discord.channels

import domain.views.channels.TextChannel
import io.github.vjames19.futures.jdk8.Future
import org.javacord.api.entity.channel.TextChannel as JTextChannel

class DiscordTextChannel(val textChannel: JTextChannel) : DiscordChannel(textChannel.asServerChannel().get()), TextChannel {
    override fun sendMessage(content: String) {
        textChannel.sendMessage(content)
    }

    override fun getMessage(): String {
        return Future {
            var message: String? = null
            val listener = textChannel.addMessageCreateListener { createEvent ->
                if (!createEvent.message.userAuthor.get().isBot)
                    message = createEvent.message.content
            }
            while (message == null)
                Thread.sleep(50)
            listener.remove()
            message ?: "N.A"
        }.get()
    }
}