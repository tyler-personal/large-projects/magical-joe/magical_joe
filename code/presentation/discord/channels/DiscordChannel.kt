package presentation.discord.channels

import domain.entities.enums.UserChannelPermission
import domain.entities.enums.UserChannelPermission.*
import domain.views.channels.Channel
import domain.views.servers.Server
import domain.views.users.User
import org.javacord.api.entity.channel.ServerChannel as JServerChannel
import org.javacord.api.entity.permission.PermissionType
import org.javacord.api.entity.permission.PermissionsBuilder
import presentation.discord.DiscordServer
import java.util.*

open class DiscordChannel(private val channel: JServerChannel) : Channel {
    override val name: String get() = channel.name
    override val id: String get() = channel.idAsString
    override val server: Server get() = DiscordServer(channel.server)

    override fun enablePermissions(user: User, vararg permissions: UserChannelPermission) {
        updatePermissions(user, PermissionsBuilder().setAllowed(*permissions.asJPermissions))
    }

    override fun disablePermissions(user: User, vararg permissions: UserChannelPermission) {
        updatePermissions(user, PermissionsBuilder().setDenied(*permissions.asJPermissions))
    }

    private fun updatePermissions(user: User, builder: PermissionsBuilder) {
        channel.createUpdater().addPermissionOverwrite(
            channel.api.getUserById(user.id).join(),
            builder.build()
        ).update()
    }

    private val Array<out UserChannelPermission>.asJPermissions get() = map { permission ->
        when (permission) {
            READ_MESSAGES -> PermissionType.READ_MESSAGES
            SEND_MESSAGES -> PermissionType.SEND_MESSAGES
            CONNECT_TO_VOICE -> PermissionType.CONNECT
        }
    }.toTypedArray()

    override fun hashCode() = Objects.hash(id)

    override fun equals(other: Any?) = when {
        other === this -> true
        other is DiscordChannel -> other.id == id
        else -> false
    }
}