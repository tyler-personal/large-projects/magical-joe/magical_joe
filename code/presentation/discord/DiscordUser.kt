package presentation.discord

import domain.entities.enums.UserStatus
import domain.views.channels.VoiceChannel
import domain.views.events.MessageEvent
import domain.views.servers.Server
import domain.views.servers.VoiceServer
import domain.views.users.VoiceUser
import io.github.vjames19.futures.jdk8.Future
import presentation.discord.events.DiscordMessageEvent
import java.util.*
import org.javacord.api.entity.user.User as JUser
import org.javacord.api.entity.user.UserStatus as JUserStatus
import org.javacord.api.entity.channel.Channel as JChannel
import org.javacord.api.entity.server.Server as JServer

class DiscordUser(private val user: JUser): VoiceUser {
    override var name: String = user.name
        set(value) {
            user.mutualServers.forEach { user.updateNickname(it, value) }
            field = value
        }
    override val id: String = user.idAsString
    override val isBot: Boolean = user.isBot
    override val status: UserStatus = when (user.status) {
        JUserStatus.ONLINE -> UserStatus.ACTIVE
        JUserStatus.IDLE -> UserStatus.IDLE
        JUserStatus.OFFLINE -> UserStatus.OFFLINE
        else -> UserStatus.IDLE
    }

    override val servers: List<VoiceServer> get() = user.mutualServers.map(::DiscordServer)
    override val textChannels get() = servers.flatMap { it.textChannels }
    override val voiceChannels get() = servers.flatMap { server -> server.voiceChannels }

    override fun move(channel: VoiceChannel) {
        user.api.getServerVoiceChannelById(channel.id).orElse(null)?.let {
            user.move(it)
        }
    }

    override fun sendMessage(content: String) {
        user.sendMessage(content)
    }

    override fun privateMessage(): MessageEvent {
        return Future {
            var result: MessageEvent? = null
            user.addMessageCreateListener { event ->
                result = DiscordMessageEvent(event)
            }
            while (result == null)
                Thread.sleep(50)
            result!!
        }.get()
    }

    override fun getNickname(server: Server): String? {
        val id = server.id.toLongOrNull() ?: return null
        return user.getNickname(user.mutualServers.find { it.id == id })
                .orElse(null)
    }

    override fun hashCode() = Objects.hash(id)

    override fun equals(other: Any?) = when {
        other === this -> true
        other is DiscordUser -> other.id == id
        else -> false
    }
}