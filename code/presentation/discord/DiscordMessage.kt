package presentation.discord

import domain.views.Message
import org.javacord.api.entity.message.Message as JMessage

class DiscordMessage(private val message: JMessage) : Message {
    override val id: String get() = message.idAsString
    override val content: String get() = message.content
    override val authorID: String get() = message.author.idAsString
}