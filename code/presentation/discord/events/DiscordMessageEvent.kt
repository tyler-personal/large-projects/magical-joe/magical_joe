package presentation.discord.events

import domain.interactors.ImageBytesFromURL
import domain.views.SimpleMessage
import domain.views.channels.TextChannel
import domain.views.events.MessageEvent
import domain.views.servers.Server
import domain.views.users.User
import lib.tryOrNull
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.embed.EmbedBuilder
import org.javacord.api.event.message.MessageCreateEvent
import presentation.discord.DiscordServer
import presentation.discord.channels.DiscordTextChannel
import presentation.discord.DiscordUser
import java.util.*

open class DiscordMessageEvent(private val event: MessageCreateEvent) : MessageEvent {
    override val id: String get() = event.message.idAsString
    override val message: String get() = event.message.content
    override val user: User get() = DiscordUser(event.message.author.asUser().get())

    override val server: Server? get() = event.channel.asServerChannel().orElse(null)?.server?.let(::DiscordServer)
    override val channel: TextChannel? get() = tryOrNull { event.channel?.let(::DiscordTextChannel) }
    override val mentionedUsers: List<User> = event.message.mentionedUsers.map(::DiscordUser)
    override fun deleteMessage() {
        event.message.delete().join()
    }

    override fun displayPictureByUrl(url: String) {
        tryOrNull { event.channel.sendMessage(
            EmbedBuilder().setImage(ImageBytesFromURL.perform(url))
        )} ?: channel?.sendMessage("Couldn't read bytes: $url")
    }

    override fun reply(content: String): SimpleMessage? =
        privateReply(content)

    override fun replyWithCode(content: String, language: String): SimpleMessage? =
        privateReply("```$language\n$content\n```")

    override fun replyWithNamedLink(title: String, url: String) {
        event.channel.sendMessage(EmbedBuilder().setDescription("[$title]($url)"))
    }

    private fun privateReply(s: String) = event.channel.sendMessage(s).join()?.let(::SimpleDiscordMessage)

    private class SimpleDiscordMessage(private val msg: Message) : SimpleMessage {
        override val id: String = msg.idAsString
        override val message: String = msg.content
        override fun appendMessage(x: String) {
            msg.edit("$message$x")
        }
    }

    override val String.bold: String get() = "**$this**"
    override val String.italics: String get() = "*$this*"
    override val String.codeSnip: String get() = "`$this`"
    override val String.hideURL: String get() = "<$this>"

    override fun hashCode() = Objects.hash(id)

    override fun equals(other: Any?) = when {
        other === this -> true
        other is DiscordMessageEvent -> other.id == id
        else -> false
    }
}