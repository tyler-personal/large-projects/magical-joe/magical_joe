package presentation.discord.events

import domain.views.events.ImageCommandEvent
import org.javacord.api.event.message.MessageCreateEvent

class DiscordImageCommandEvent(
    event: MessageCreateEvent,
    command: String,
    override val messageAsBytes: ByteArray
) : DiscordCommandEvent(event, command), ImageCommandEvent