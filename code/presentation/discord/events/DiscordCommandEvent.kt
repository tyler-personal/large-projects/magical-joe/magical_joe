package presentation.discord.events

import domain.views.events.CommandEvent
import org.javacord.api.event.message.MessageCreateEvent

open class DiscordCommandEvent(
    event: MessageCreateEvent,
    override val command: String
) : DiscordMessageEvent(event), CommandEvent {
    override val message: String = event.message.content
}