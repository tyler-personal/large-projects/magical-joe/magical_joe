package presentation.discord

import domain.views.servers.VoiceServer
import presentation.discord.channels.DiscordTextChannel
import presentation.discord.channels.DiscordVoiceChannel
import java.util.*
import org.javacord.api.entity.server.Server as JavacordServer

class DiscordServer(private val server: JavacordServer) : VoiceServer {
    override val name: String get() = server.name
    override val id: String get() = server.idAsString

    override val afkChannel get() = server.afkChannel.orElse(null)?.let(::DiscordVoiceChannel)
    override val textChannels get() = server.textChannels.map(::DiscordTextChannel)
    override val voiceChannels get() = server.voiceChannels.map(::DiscordVoiceChannel)

    override fun hashCode() = Objects.hash(id)

    override fun equals(other: Any?) = when {
        other === this -> true
        other is DiscordServer -> other.id == id
        else -> false
    }
}