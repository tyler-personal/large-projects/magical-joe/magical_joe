package presentation

import io.javalin.Context
import io.javalin.Javalin
import io.javalin.rendering.JavalinRenderer
import io.javalin.rendering.template.JavalinMustache
import lib.merge
import domain.entities.enums.UserChannelPermission.*
import domain.views.bots.WebBot
import java.util.*

class JavalinRouter(val bot: WebBot) {
    private val herokuPort = ProcessBuilder().environment()["PORT"]?.toInt() ?: 7000
    private val viewLocation = "presentation/discord/web"

    private val app = Javalin.create()

    private val paths = mutableMapOf<String, Path>()
    private abstract inner class Path(val name: String, val url: String) {
        val nav get() = mapOf("navbar" to navbarList(this))
    }

    private inner class Get(
        name: String,
        url: String,
        file: String,
        paramsBlock: (Context) -> Map<String, Any> = { mapOf() }
    ) : Path(name, url) {
        init {
            paths[name] = this
            app.get(url) {
                it.render("$viewLocation/$file", nav.merge(paramsBlock(it)))
            }
        }
    }

    private val tokensByUserID = mutableMapOf<String, String>()

    init {
        JavalinRenderer.register(JavalinMustache, ".hbs")
        app.enableStaticFiles(viewLocation)
        app.start()
        Get("Home", "/", "index.hbs")

        fun preferencesParams(ctx: Context): Map<String, Any> {
            val user = ctx.discordUser

            return mapOf(
                "name" to (user?.name ?: "N.A"),
                "id" to (user?.id ?: "-1"),
                "channelsByServer" to (user?.servers?.map { server ->
                    mapOf(
                        "server" to server.name,
                        "server_id" to server.id,
                        "channels" to server.channels.map { channel ->
                            mapOf(
                                "name" to channel.name,
                                "id" to channel.id,
                                "in" to (channel in user.channels)
                            )
                        }
                    )
                } ?: listOf())
            )
        }
        val preferences = Get("Preferences", "/preferences", "preferences.hbs", ::preferencesParams)

        app.get("/preferences/:discord_id") { ctx ->
            ctx.pathParam("discord_id").let { id ->
                ctx.cookieStore("magical_joe_discord_id", id)
            }
            ctx.render("$viewLocation/preferences.hbs", preferences.nav.merge(preferencesParams(ctx)))
        }

        app.get("/generate-token") { ctx ->
            val token = UUID.randomUUID().toString()
            ctx.discordUser?.let {
                tokensByUserID[it.id] = token
                it.sendMessage(token)
            }
        }

        app.post("/new-preferences") { ctx ->
            val user = ctx.discordUser!!

            if (ctx.formParam("token") != tokensByUserID[user.id]) {
                ctx.result("invalid")
                return@post
            }
            ctx.result("valid")

            val channelsByServer = ctx.formParamMap()
                .filterNot { listOf("name", "id", "token").contains(it.key) }
                .mapNotNull {
                    val args = it.key.split("_")
                    val channelID = args[0]
                    val serverID = args[1]

                    val channel = bot.getChannelByIdForRouting(channelID)

                    serverID to channel
                }.groupBy { it.first }.map { bot.getServerByIdForRouting(it.key) to it.value.map { it.second } }.toMap()

            ctx.formParam("name")?.let { user.name = it }
            channelsByServer.keys.forEach { server ->
                val selectedChannelIDs = channelsByServer[server]?.mapNotNull { it?.id } ?: listOf()
                val restrictedChannelIDs = listOf(
                    154028046851112961, 209053357904625664, 236596906917101569, 303349082154598401, 390250270074077196
                ).map { it.toString() }

                server?.channels?.forEach { channel ->
                    when {
                        channel.id in selectedChannelIDs && channel.id in restrictedChannelIDs ->
                            channel.enablePermissions(user, READ_MESSAGES)
                        channel.id in selectedChannelIDs ->
                            channel.enablePermissions(user, SEND_MESSAGES, READ_MESSAGES, CONNECT_TO_VOICE)
                        else ->
                            channel.disablePermissions(user, SEND_MESSAGES, READ_MESSAGES, CONNECT_TO_VOICE)
                    }
                }
            }
        }
    }

    private fun navbarList(path: Path) = paths.keys.map {
        mapOf(
            "name" to it,
            "class" to "item ${if (path.name == it) "active" else ""}",
            "path" to paths[it]?.url
        )
    }

    private val Context.discordUser get() =
        cookieStore<String>("magical_joe_discord_id").let { bot.getUserById(it) }
}