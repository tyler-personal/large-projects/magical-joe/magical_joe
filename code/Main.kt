import data.local.json.ServerPreferencesJSON
import data.local.json.UserJSON
import data.local.json.WoWStatisticsJSON
import data.local.memory.WoWAuctionMemoryStore
import data.remote.*
import data.remote.BibleAPI
import data.remote.IsThereAnyDealAPI
import data.remote.TextAnalysisAPI
import data.remote.TheMovieDB
import data.remote.albion.AlbionAPI
import data.remote.blizzard.WoWAuctionAPI
import data.remote.blizzard.WoWCharacterAPI
import data.remote.blizzard.WoWItemAPI
import data.remote.google.GoogleImageAnnotator
import data.repositories.api.floating.GenderAPI
import data.repositories.api.google.GoogleChartMaker
import domain.*
import domain.boundaries.datastores.BibleDatastore
import domain.boundaries.datastores.TextDatastore
import domain.boundaries.datastores.images.ChartDatastore
import domain.datastores.*
import domain.datastores.games.*
import domain.entities.DatastoreContainer
import presentation.DiscordBot
import presentation.JavalinRouter
import presentation.telegram.TelegramBot
import java.io.OutputStream
import java.io.PrintStream
import java.io.IOException
import java.util.logging.LogManager


class Main { // Setup this way for Maven deployment
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            log("Starting main")
            // destroyLoggersAndSystemOut()
            val container = object : DatastoreContainer {
                override val wowItems: WoWItemDatastore = WoWItemAPI()
                override val wowCharacters: WoWCharacterDatastore = WoWCharacterAPI()
                override val steam: SteamDatastore = IsThereAnyDealAPI()

                override val wowAuction = object : WoWAuctionDatastore {
                    override val local: WoWAuctionDatastore.Local = WoWAuctionMemoryStore()
                    override val remote: WoWAuctionDatastore.Remote = WoWAuctionAPI()
                    override val stats: WoWAuctionDatastore.Statistics = WoWStatisticsJSON(remote, wowItems)
                }

                override val userDatastore: UserDatastore = UserJSON()
                override val serverPrefs: ServerPreferencesDatastore = ServerPreferencesJSON()

                override val bible: BibleDatastore = BibleAPI()
                override val gender: GenderDatastore = GenderAPI()
                override val textAnalyzer: TextDatastore = TextAnalysisAPI()

                override val chartMaker: ChartDatastore = GoogleChartMaker()
                override val imageDatastore: ImageDatastore = GoogleImageAnnotator()

                override val siteUrl: String = "https://magical-joe.serveo.net"

                override val albion: AlbionDatastore = AlbionAPI()
                override val actor: ActorDatastore = TheMovieDB()
                override val heroes: HeroesDatastore = IcyVeins()

                override val polls: PollDatastore
                    get() = TODO("not implemented")
            }

            val discord = DiscordBot()
            startWithVoice(discord, container)
            log("~~ DISCORD BOT STARTED ~~")

            val telegram = TelegramBot()
            start(telegram, container)
            log("~~ TELEGRAM BOT STARTED ~~")

            JavalinRouter(discord)
            log("~~ WEB BOT STARTED ~~")
        }
    }
}



val consoleOut: PrintStream = System.out
fun log(x: Any) = consoleOut.println(x)

fun destroyLoggersAndSystemOut() {
    LogManager.getLogManager().reset() // Destroys logger for other libraries.
    System.setOut(PrintStream(object : OutputStream() {
        override fun write(b: Int) {}
    }))
}