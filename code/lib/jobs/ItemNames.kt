package lib.jobs
import domain.entities.DatastoreContainer
import khttp.get
import lib.mapAndTrackPercent
import org.json.JSONObject
import java.io.File

fun main(repos: DatastoreContainer) {
    val repo = repos.wowItems
    val url = "https://us.api.battle.net/wow/auction/local/Bleeding-Hollow?apikey=g8z93hzgvfrj3yfdjdwxmfe8buxzj4db"
    val auctionURL = get(url).jsonObject.getJSONArray("files").getJSONObject(0).getString("url")

    val ids = get(auctionURL).jsonObject.getJSONArray("auctions").map { it as JSONObject }.map { jObj ->
        jObj.getInt("item")
    }.distinct()

    val idToName = ids.mapAndTrackPercent(::println) { id ->
        id to (repo.getItemName(id)?.replace("\"", "") ?: "N.A")
    }.toMap()

    File("code/data/local/raw/itemIDToName.json").writeText(
        "[" + idToName.entries.joinToString(", ") { "{ \"id\": ${it.key}, \"name\": \"${it.value}\" }" } + "]"
    )
}
