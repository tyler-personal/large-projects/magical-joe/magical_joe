package lib.jobs

import data.local.databases.Database
import org.intellij.lang.annotations.Language

fun main() {
    val migrations = listOf(m4)
    val db = Database()
    migrations.forEach { db.executeSQL(it) }
}

@Language("SQL") val m1 = """
  CREATE TABLE auctions(
    itemID bigint,
    min bigint,
    25Median bigint,
    10Median bigint,
    count int,
    modifiedEpoch bigInt,
    server varchar(255)
  )
""".trimIndent()

@Language("SQL") val m2 = """
  CREATE TABLE users(
    name varchar(255),
    country varchar(255),
    channel varchar(255),
    platform varchar(255),
    wowServer varchar(255),

    imageAnalysis bool
  )
""".trimIndent()

@Language("SQL") val m3 = """
    ALTER TABLE users ADD uniqueName varchar(255)
""".trimIndent()

@Language("SQL") val m4 = "ALTER TABLE users DROP country"

@Language("SQL") val m5 = "ALTER TABLE users ADD channelID long"


// SELECT table_schema, table_name, data_length, index_length FROM information_schema.tables WHERE table_name='auctions'