package lib.jobs

import com.google.cloud.vision.v1.*
import com.google.cloud.vision.v1.Feature.Type
import com.google.protobuf.ByteString
import java.io.File
import java.util.*

fun main() {
    val bytes = Base64.getEncoder().encodeToString(File("code/src/lib.jobs/test.jpg").readBytes())
    val key = "AIzaSyCg5D9SlrQJuur72H9411YdhSdVP-dbsmk"
    val response = khttp.post("https://vision.googleapis.com/v1/images:annotate?key=$key", data="""
        {
          "requests":[{
              "image":{"message":"$bytes"},
              "bots":[
                {"type":"LABEL_DETECTION", "maxResults":3},
                {"type":"WEB_DETECTION", "maxResults": 3}
              ]
          }]
        }
    """.trimIndent())
    ImageAnnotatorClient.create().use { vision ->
        val imgBytes = ByteString.copyFrom(File("code/src/lib.jobs/test.jpg").readBytes())

        val requests = listOf(
            AnnotateImageRequest.newBuilder()
                .addFeatures(Feature.newBuilder().setType(Type.LABEL_DETECTION).build())
                .setImage(Image.newBuilder().setContent(imgBytes).build())
                .build()
        )
        val responses = vision.batchAnnotateImages(requests).responsesList

        println("Error count: ${responses.count { it.hasError() }}")
        responses.filter { !it.hasError() }.forEach { res ->
            res.webDetection.allFields.forEach { k, v -> println("$k : $v") }
            res.labelAnnotationsList.forEach { it.allFields.forEach { k, v -> println("$k : $v") } }
        }
    }
}