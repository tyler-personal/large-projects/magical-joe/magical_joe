package lib

class Either<L, R> {
    var left: L? = null
    var right: R? = null
    companion object {
        fun <L, R> left(l: L) = Either<L, R>().also { it.left = l }
    }
}