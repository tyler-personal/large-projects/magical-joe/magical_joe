package lib

import data.local.json.JSON
import domain.interactors.EpochDateDistanceConverter
import org.json.*
import java.io.BufferedWriter
import java.math.BigInteger
import java.sql.ResultSet

// TODO: Upload my ExtensionFunctions project to maven and replace this.
fun <T> tryOrNull(block: () -> T?): T? = try { block() } catch (e: Exception) { null }
fun <T> ifOrNull(conditional: Boolean, block: () -> T?): T? = if (conditional) block() else null

inline fun <T, Q> Iterable<Pair<T, Q>>.forEachPair(block: (T, Q) -> Unit) { this.forEach { block(it.first, it.second) } }

fun <T, Q> T.tap(block: (T) -> Q): T { block(this); return this }

fun <T, Q> T?.tapIfNotNull(block: (T) -> Q): T? {
    if (this != null)
        block(this)

    return this
}


fun <T, Q> T.change(block: (T) -> Q): Q = block(this)
fun <T, Q> T?.changeNotNull(block: (T) -> Q): Q? = if (this != null) block(this) else null

fun <T, Q> Iterable<T>.mapAndTrackPercent(output: (String) -> Unit, block: (T) -> Q): List<Q> {
    var counter = 0
    var percent = 0
    return this.map {
        counter += 1
        if (counter >= (this.count() / 100.0)) {
            counter = 0
            percent += 1
            output("$percent% done")
        }
        block(it)
    }
}

fun String.decapitalize() = this[0].toLowerCase() + substring(1)
fun String.safeURL() = this.replace(" ", "%20")
fun String.capitalizeWords() = this.split(" ").joinToString(" ") { it.capitalize() }
fun String.hasSurrounding(delimiter: String) = this.startsWith(delimiter) && this.endsWith(delimiter)
fun String.replaceSurrounding(old: String, new: String) =
    if (this.hasSurrounding(old))
        new + this.removeSurrounding(old) + new
    else
        this

fun String.replaceSurrounding(oldLeft: String, oldRight: String, newLeft: String, newRight: String) =
    if (this.startsWith(oldLeft) && this.endsWith(oldRight))
        newLeft + this.removePrefix(oldLeft).removePrefix(oldRight) + newRight
    else
        this

fun String.spacesBeforeCapitals() = split("(?=[A-Z])".toRegex()).join()
fun String.between(before: String, after: String): String {
    val startIndex = indexOf(before) + before.length
    val endIndex = substring(startIndex).indexOf(after) + startIndex
    return substring(startIndex, endIndex)
}
fun String.titleize() = replace("-", " ").replace("_", " ").capitalizeWords()
fun String.replace(vararg pairs: Pair<String, String>) = pairs.fold(this) { acc, e -> acc.replace(e.first, e.second) }
fun String.remove(vararg s: String) = s.fold(this) { acc, e -> acc.replace(e, "") }


fun List<String>.join(separator: String = " ", transform: ((String) -> CharSequence)? = null) =
    joinToString(separator, transform = transform)

operator fun <T> List<T>.component6(): T {
    return get(5)
}

fun <T> List<T>.from(i: Int) = tryOrNull { subList(i, this.size - 1) }

val <T, LI: Collection<T>> LI.counter get() = groupBy { it }.mapValues { it.value.size }


fun ResultSet.getBigInteger(col: String) = this.getBigDecimal(col).toBigInteger()
fun Double.toBigInteger() = this.toBigDecimal().toBigInteger()

val JSONArray.jsonObjects: List<JSONObject> get() = 0.until(this.length()).map { getJSONObject(it) }

fun <T> List<T>.mapIf(block: (T) -> T?): List<T> = map { block(it) ?: it }
fun <T> List<T>.splitAt(index: Int) = listOf(subList(0, index + 1), subList(index + 1, size))
fun <T> List<T>.splitAtLast(block: (T) -> Boolean) = splitAt(indexOfLast(block))

fun <T, Q> Map<T, Q>.merge(map: Map<T, Q>): Map<T, Q> {
    return this.toMutableMap().apply {
        putAll(map)
    }.toMap()
}

val BigInteger.epochTimeFromNow get() =
    EpochDateDistanceConverter.perform(System.currentTimeMillis().toBigInteger(), this)

fun Double.epochToHours(): Double = this / 1000.0 / 3600.0

val Process.inputLines get() = inputStream.bufferedReader().lineSequence()
val Process.errorLines get() = errorStream.bufferedReader().lineSequence()

val String.isHTTP get() = startsWith("http") || startsWith("https")
val String.isIMG get() = listOf("png", "jpg", "gif", "webm").any { endsWith(".$it") }

fun BufferedWriter.writeLine(content: String, flush: Boolean = false) {
    write(content)
    newLine()
    if (flush)
        flush()
}

//class TernaryIntermediate<T>(private val condition: Boolean, private val first: T) {
//    infix fun `!`(second: T) = if (condition) first else second
//}
//infix fun <T> Boolean.`?`(t: T) = TernaryIntermediate(this, t)