package domain.entities

class PersistentThread(milliSeconds: Long? = null, seconds: Long? = null, minutes: Long? = null,
                       block: () -> Unit) {
    private val time = (milliSeconds ?: 0) + ( (seconds ?: 0) * 1000) + ( (minutes ?: 0) * 1000 * 60)

    private val thread = Thread {
        while (true) {
            block()
            Thread.sleep(time)
        }
    }

    fun start() = thread.start()
}