package domain.entities

import domain.boundaries.datastores.*
import domain.boundaries.datastores.images.*
import domain.datastores.*
import domain.datastores.games.*

interface DatastoreContainer {
    val siteUrl: String

    val wowCharacters: WoWCharacterDatastore
    val wowItems: WoWItemDatastore
    val wowAuction: WoWAuctionDatastore

    val steam: SteamDatastore
    val albion: AlbionDatastore
    val heroes: HeroesDatastore

    val chartMaker: ChartDatastore
    val bible: BibleDatastore
    val textAnalyzer: TextDatastore
    val polls: PollDatastore
    val imageDatastore: ImageDatastore

    val userDatastore: UserDatastore
    val serverPrefs: ServerPreferencesDatastore

    val gender: GenderDatastore
    val actor: ActorDatastore
}