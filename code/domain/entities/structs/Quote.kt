package domain.entities.structs


class Quote(val id: Int, val value: String, val adderUserID: String, val authorUserID: String)