package domain.entities.structs.games

import java.math.BigInteger

data class WoWAuctionServer(
    val name: String,
    val lastUpdatedEpoch: BigInteger,
    val items: List<WoWAuctionItem>
) {
    val itemsByID: Map<Int, WoWAuctionItem> = items.map { it.id to it }.toMap()
    val itemsByName = mapOf<String, WoWAuctionItem>()
    val itemNames = listOf<String>()

    val nameWithSpaces = name.replace("-", " ")
}