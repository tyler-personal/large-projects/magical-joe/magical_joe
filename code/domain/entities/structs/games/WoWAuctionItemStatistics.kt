package domain.entities.structs.games

import java.math.BigInteger

class WoWAuctionItemStatistics(
    val name: String,
    val epochTimeToBuyouts: Map<Int, List<BigInteger>>
)