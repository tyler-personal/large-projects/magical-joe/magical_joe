package domain.entities.structs.games

import java.math.BigInteger

class WoWCoins(val total: BigInteger) {
    enum class Type {
        GOLD, SILVER, COPPER
    }
    val gold = total / 10000.toBigInteger()
    val silver = (total.toBigDecimal() % 10000.toBigDecimal()).toBigInteger() / 100.toBigInteger()
    val copper = (total.toBigDecimal() % 100.toBigDecimal()).toBigInteger()

    override fun toString() = "${gold}G ${silver}S ${copper}C"

    val mainCoin get() = when {
        gold > 5.toBigInteger() -> Type.GOLD
        silver > 5.toBigInteger() -> Type.SILVER
        else -> Type.COPPER
    }

    fun getTotalByType(type: Type) = when(type) {
        Type.GOLD -> gold
        Type.SILVER -> (gold * 100.toBigInteger()) + silver
        Type.COPPER -> (gold * 10000.toBigInteger()) + (silver * 100.toBigInteger()) + copper
    }.toInt()
}