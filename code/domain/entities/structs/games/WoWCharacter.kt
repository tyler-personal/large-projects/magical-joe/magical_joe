package domain.entities.structs.games

data class WoWCharacter(val name: String, val itemLevel: Int)