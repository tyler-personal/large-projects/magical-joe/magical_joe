package domain.entities.structs.games.albion

class AlbionItem(val name: String, val description: String?, private val priceData: Map<AlbionLocation, Price>) {
    data class Price(val sellMin: Int, val buyMin: Int, val sellMax: Int, val buyMax: Int)

    fun getPrice(location: AlbionLocation): Price? = priceData[location]

    override fun toString() = name
}