package domain.entities.structs.games.albion

enum class AlbionLocation {
    FOREST_CROSS, MOUNTAIN_CROSS, HIGHLAND_CROSS, SWAMP_CROSS, STEPPE_CROSS, // Starting zones
    THETFORD, LYMHURST, BRIDGEWATCH, MARTLOCK, FORT_STERLING, // Mid zones
    CAERLEON, BLACK_MARKET, // Late zones
    UNKNOWN
}