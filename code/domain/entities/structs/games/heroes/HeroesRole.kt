package domain.entities.structs.games.heroes

enum class HeroesRole {
    Specialist, Assassin, Warrior, Support, Multiclass
}