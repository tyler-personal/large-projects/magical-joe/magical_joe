package domain.entities.structs.games.heroes

import domain.datastores.games.HeroesDatastore


data class Hero(
    val name: String, val role: HeroesRole,
    val synergyNames: List<String>, val counterNames: List<String>, val abilities: List<Ability>,
    val repo: HeroesDatastore
) {
    val synergies get() = synergyNames.map(repo::getHeroInfo)
    val counters get() = counterNames.map(repo::getHeroInfo)

    override fun toString() = name
}