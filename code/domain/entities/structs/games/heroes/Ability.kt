package domain.entities.structs.games.heroes

enum class HotKey {
    Q, W, E, R, D, PASSIVE_TRAIT, Z
}
data class Ability(val name: String, val levelRequired: Int, val hotkey: HotKey, val description: String)