package domain.entities.structs.games

import lib.changeNotNull
import java.math.BigInteger

data class WoWAuctionItem(
    val id: Int, val modified: BigInteger, val count: Int,
    val minCostInt: BigInteger?, val bottom25Int: BigInteger?, val bottom10Int: BigInteger?
) {

    val bottom25Median: WoWCoins? = bottom25Int.changeNotNull { WoWCoins(it) }
    val bottom10Median: WoWCoins? = bottom10Int.changeNotNull { WoWCoins(it) }
    val minCost: WoWCoins? = minCostInt.changeNotNull { WoWCoins(it) }

    init {
        // bottom25MedianInt = sortedBuyouts.getOrNull(buyouts.size/4)
        // bottom10MedianInt = buyouts.getOrNull(buyouts.size/10)
    }


}