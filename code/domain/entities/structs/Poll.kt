package domain.entities.structs

class Poll(
    val name: String,
    private val timeLeftInMinutes: () -> Double,
    private val retrieveCountBlock: () -> Int
) {
    private var lastCount = 0

    val count get() = when {
        timeLeftInMinutes() <= 0 -> lastCount
        else -> retrieveCountBlock()
    }

    fun doRecount() {
        lastCount = retrieveCountBlock()
    }
}