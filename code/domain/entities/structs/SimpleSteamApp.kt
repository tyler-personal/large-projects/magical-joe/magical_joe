package domain.entities.structs

class SimpleSteamApp(
    val name: String,
    val id: Int,
    val fetch: () -> SteamApp?
)