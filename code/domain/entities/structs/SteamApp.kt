package domain.entities.structs


class SteamApp(
    val id: Int,
    val name: String,
    val lowestPrice: Double,
    var price: Double
) {
    override fun toString() = "$name: steam://store/$id @ $$price"

}