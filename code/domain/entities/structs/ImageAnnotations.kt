package domain.entities.structs

class ImageAnnotations(val labels: List<Result>?, val web: List<Result>?,
                       val urlsWithImage: List<String>?, val similarImageURLs: List<String>?) {
    class Result(val description: String?, val score: Double?)
}