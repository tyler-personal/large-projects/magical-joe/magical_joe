package domain.entities.structs.movies

class ActorQuery(val name: String) {
    override fun toString() = name
}