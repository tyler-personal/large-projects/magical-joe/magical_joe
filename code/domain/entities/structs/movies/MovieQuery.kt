package domain.entities.structs.movies

class MovieQuery(val name: String) {
    override fun toString() = name
}