package domain.entities.structs.movies

class ActorResult(val name: String, val id: Int, val queryName: String, val movieIDs: List<Int>)