package domain.entities.structs.movies

class MovieResult(val name: String, val id: Int, val queryName: String, val actorIDs: List<Int>)