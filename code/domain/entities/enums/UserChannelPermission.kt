package domain.entities.enums

enum class UserChannelPermission {
    SEND_MESSAGES, READ_MESSAGES, CONNECT_TO_VOICE
}