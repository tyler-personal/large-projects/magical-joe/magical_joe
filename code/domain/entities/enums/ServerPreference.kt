package domain.entities.enums

import kotlin.reflect.KClass

sealed class ServerPreference<T: Any>(val type: KClass<T>)
object VotingChannelID : ServerPreference<String>(String::class)

