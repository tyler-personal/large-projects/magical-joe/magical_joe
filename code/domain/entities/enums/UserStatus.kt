package domain.entities.enums

enum class UserStatus {
    IDLE, ACTIVE, OFFLINE
}