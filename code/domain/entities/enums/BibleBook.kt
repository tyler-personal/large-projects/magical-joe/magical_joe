package domain.core.enums

enum class BibleBook {
    Genesis, Exodus, Leviticus, Numbers, Deuteronomy, Joshua, Judges, Ruth, Samuel1, Samuel2, Kings1, Kings2,
    Chronicles1, Chronicles2, Ezra, Nehemiah, Esther, Job, Psalm, Proverbs, Ecclesiastes, Song_of_Songs, Isaiah,
    Jeremiah, Lamentations, Ezekiel, Daniel, Hosea, Joel, Amos, Obadiah, Jonah, Micah, Nahum, Habakkuk, Zephaniah,
    Haggai, Zechariah, Malachi, Tobit, Judith, The_Wisdom_of_Solomon, Sirach, Baruch, Letter_of_Jeremiah,
    Song_of_Three_Young_Men, Susanna, Bel_and_the_Dragon, Maccabees1, Maccabees2, Esdras1, Esdras2,
    The_Prayer_of_Manasseh, Matthew, Mark, Luke, John, Acts, Romans, Corinthians1, Corinthians2, Galatians, Ephesians,
    Philippians, Colossians, Thessalonians1, Thessalonians2, Timothy1, Timothy2, Titus, Philemon, Hebrews, James,
    Peter1, Peter2, John1, John2, John3, Jude, Revelation,
}