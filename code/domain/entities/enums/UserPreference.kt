package domain.core.enums

enum class UserPreference {
    ShowImage, DeleteCommands, NormalizedSearch
}