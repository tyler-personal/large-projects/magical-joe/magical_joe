package domain

import domain.commands.*
import domain.commands.democracy.votingCommands
import domain.commands.games.*
import domain.commands.queries.movieCommands
import domain.entities.DatastoreContainer
import domain.views.bots.*
import domain.views.bots.plugins.VoicePlugin

fun <T : SimpleBot> start(bot: T, repos: DatastoreContainer) {
    listOf(
        ::albionCommands, ::heroesCommands, ::imageCommands, ::preferenceCommands,
        ::scriptCommands, ::simpleCommands, ::steamCommands, ::wowCommands, ::movieCommands
    ).forEach { it(bot, repos) }

    if (bot is QuoteBot) {
        quoteCommands(bot, repos)
        userSpecificCommand(bot, repos)
    }
    if (bot is VotingBot) {
        votingCommands(bot, repos)
    }
}


fun startWithVoice(bot: VoicePlugin, repos: DatastoreContainer) {
    start(bot, repos)
    voiceCommands(bot, repos)
}



