package domain.views.users

import domain.entities.enums.UserStatus
import domain.views.channels.Channel
import domain.views.channels.TextChannel
import domain.views.events.MessageEvent
import domain.views.servers.Server

interface User {
    var name: String
    val id: String
    val status: UserStatus

    val isBot: Boolean
    val isPerson: Boolean get() = !isBot

    val servers: List<Server>
    val textChannels: List<TextChannel>

    val channels: List<Channel> get() = textChannels

    fun sendMessage(content: String)
    fun privateMessage(): MessageEvent

    fun getNickname(server: Server): String?
}