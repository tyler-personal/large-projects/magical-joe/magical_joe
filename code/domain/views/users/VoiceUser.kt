package domain.views.users

import domain.views.channels.Channel
import domain.views.channels.VoiceChannel

interface VoiceUser : User {
    fun move(channel: VoiceChannel)

    val voiceChannels: List<VoiceChannel>
    override val channels: List<Channel> get() = voiceChannels + textChannels
}