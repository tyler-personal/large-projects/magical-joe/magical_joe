package domain.views.bots.plugins

import domain.views.users.User

interface FindUserPlugin {
    fun getUserByName(name: String): User?
    fun getUserById(id: String): User?

    fun getUserByIdOrName(nameOrId: String) =
        getUserById(nameOrId) ?: getUserByName(nameOrId)
}