package domain.views.bots.plugins

import domain.views.bots.SimpleBot
import domain.views.servers.VoiceServer
import domain.views.users.VoiceUser

interface VoicePlugin : SimpleBot {
    val servers: List<VoiceServer>

    fun getUserById(id: String): VoiceUser?
}