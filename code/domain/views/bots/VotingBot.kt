package domain.views.bots

import domain.views.bots.SimpleBot
import domain.views.channels.TextChannel

interface VotingBot : SimpleBot {
    fun getTextChannelById(id: String): TextChannel?
}