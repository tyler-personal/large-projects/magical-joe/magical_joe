package domain.views.bots

import domain.views.bots.plugins.FindUserPlugin
import domain.views.channels.Channel
import domain.views.servers.Server

interface WebBot : FindUserPlugin {
    fun getChannelByIdForRouting(id: String): Channel?
    fun getServerByIdForRouting(id: String): Server?
}