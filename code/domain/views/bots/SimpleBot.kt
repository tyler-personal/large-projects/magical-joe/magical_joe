package domain.views.bots

import domain.views.events.*
import domain.views.bots.HelpType.*
import domain.views.users.User

import log

enum class CommandType {
    Normal, Image
}
enum class HelpType {
    Game, Query, Simple, Local, NotTracked, Test
}
val help = mapOf<HelpType, MutableMap<String, String>>(
    Simple to mutableMapOf(), Query to mutableMapOf(), Game to mutableMapOf(), Local to mutableMapOf(),
    Test to mutableMapOf()
)
val extraHelp = mutableMapOf<String, String>()

// NOTE: Hopefully the type system gets more advanced and supports defaults on reified generic parameters (or stronger type inference)
// NOTE: Could remove a lot of boiler plate (no Helper class, no 2 functions for different `block` types, etc.)
interface SimpleBot {
    fun commandListener(command: String, block: CommandEvent.() -> Unit)
    fun imageCommandListener(command: String, block: ImageCommandEvent.() -> Unit)

    private class Helper<T : CommandEvent>(
        val name: String, val description: String?, val helpType: HelpType, val minArgs: Int = 0,
        val maxArgs: Int = Int.MAX_VALUE, val includeBotMessages: Boolean = false, val block: T.() -> Unit
    ) {
        init {
            if (description != null && helpType != NotTracked)
                help[helpType]?.let { it[name] = description }
        }

        fun commandIsValid(message: String, args: List<String>, user: User) =
            message.startsWith(name) && args.size in minArgs..maxArgs && (user.isPerson || includeBotMessages)

        fun startSafeThread(f: () -> Unit) = Thread {
            try { f() } catch(e: Exception) { log(e.localizedMessage) }
        }.start()
    }

    fun command(name: String, description: String?, helpType: HelpType, minArgs: Int = 0, maxArgs: Int = Int.MAX_VALUE,
                includeBotMessages: Boolean = false, block: CommandEvent.() -> Unit) {
        Helper(name, description, helpType, minArgs, maxArgs, includeBotMessages, block).apply {
            commandListener(name) {
                if (commandIsValid(message, args, user))
                    startSafeThread { block(this) }
            }
        }
    }

    fun imageCommand(name: String, description: String?, helpType: HelpType, minArgs: Int = 0, maxArgs: Int = Int.MAX_VALUE,
                     includeBotMessages: Boolean = false, block: ImageCommandEvent.() -> Unit) {
        Helper(name, description, helpType, minArgs, maxArgs, includeBotMessages, block).apply {
            imageCommandListener(name) {
                if (commandIsValid(message, args, user))
                    startSafeThread { block(this) }
            }
        }
    }
}

