package domain.views.bots

import domain.views.Message
import domain.views.bots.SimpleBot
import domain.views.bots.plugins.FindUserPlugin
import domain.views.channels.TextChannel

interface QuoteBot : SimpleBot, FindUserPlugin {
    fun getMessageById(id: String, channel: TextChannel): Message?
}