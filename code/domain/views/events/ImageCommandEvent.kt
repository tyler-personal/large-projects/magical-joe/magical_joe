package domain.views.events

interface ImageCommandEvent : CommandEvent {
    val messageAsBytes: ByteArray
}