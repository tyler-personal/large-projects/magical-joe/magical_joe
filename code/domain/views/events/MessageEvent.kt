package domain.views.events

import domain.views.SimpleMessage
import domain.views.servers.Server
import domain.views.users.User
import domain.views.channels.Channel

// TODO: Make this extend SimpleMessage
interface MessageEvent {
    val id: String
    val message: String

    val user: User
    val server: Server?
    val channel: Channel?

    val mentionedUsers: List<User>

    fun deleteMessage()
    fun displayPictureByUrl(url: String)

    fun reply(content: String): SimpleMessage?
    fun replyWithCode(content: String, language: String = "Kotlin"): SimpleMessage?
    fun replyWithNamedLink(title: String, url: String)


    val String.hideURL: String
    val String.italics: String
    val String.bold: String
    val String.codeSnip: String
}