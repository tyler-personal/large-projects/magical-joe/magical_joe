package domain.views.events

import domain.views.channels.TextChannel

interface CommandEvent : MessageEvent {
    val command: String
    val args: List<String> get() = message.removePrefix(command).trim().split(" ").filter { it.isNotBlank() }

    override val channel: TextChannel?
}