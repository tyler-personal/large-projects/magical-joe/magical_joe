package domain.views.channels

import domain.views.users.VoiceUser

interface VoiceChannel : Channel {
    val connectedUsers: List<VoiceUser>
}