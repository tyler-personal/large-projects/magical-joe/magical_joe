package domain.views.channels

interface TextChannel : Channel {
    fun sendMessage(content: String)
    fun getMessage(): String
}