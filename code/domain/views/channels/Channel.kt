package domain.views.channels

import domain.entities.enums.UserChannelPermission
import domain.views.servers.Server
import domain.views.users.User

interface Channel {
    val name: String
    val id: String

    val server: Server

    fun enablePermissions(user: User, vararg permissions: UserChannelPermission)
    fun disablePermissions(user: User, vararg permissions: UserChannelPermission)
}