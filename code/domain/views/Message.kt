package domain.views

interface Message {
    val id: String
    val content: String

    val authorID: String
}