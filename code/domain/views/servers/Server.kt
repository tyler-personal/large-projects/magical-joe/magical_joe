package domain.views.servers

import domain.views.channels.Channel
import domain.views.channels.TextChannel

interface Server {
    val name: String
    val id: String

    val textChannels: List<TextChannel>

    val channels: List<Channel> get() = textChannels
}