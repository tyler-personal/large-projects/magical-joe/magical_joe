package domain.views.servers

import domain.views.channels.Channel
import domain.views.channels.VoiceChannel

interface VoiceServer : Server {
    val afkChannel: VoiceChannel?

    val voiceChannels: List<VoiceChannel>
    override val channels: List<Channel> get() = voiceChannels + textChannels
}