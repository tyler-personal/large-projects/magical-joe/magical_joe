package domain.views

interface SimpleMessage {
    val id: String
    val message: String

    fun appendMessage(x: String)
}