package domain.commands

import domain.entities.PersistentThread
import domain.boundaries.datastores.BibleDatastore
import domain.core.enums.UserPreference
import lib.join
import lib.tryOrNull
import domain.datastores.games.SteamDatastore
import domain.datastores.UserDatastore
import domain.entities.DatastoreContainer
import domain.entities.structs.movies.*
import domain.views.bots.*
import domain.views.bots.HelpType
import domain.views.bots.extraHelp
import domain.views.bots.help
import domain.views.events.CommandEvent
import log
import kotlin.system.measureTimeMillis

fun simpleCommands(bot: SimpleBot, repos: DatastoreContainer) {
    bot.command("!ping", "Check if the bot works", HelpType.Test) {
        reply("pong")
    }

    bot.command("PIZZA TIME!", null, HelpType.NotTracked) {
        reply("HELL YEAH!")
    }

    var timeSinceStart = 0
    PersistentThread(seconds = 1) { timeSinceStart += 1 }.start()

    bot.command("!uptime", "Shows the amount of time since the bot was started.", HelpType.Simple) {
        reply(when {
            timeSinceStart < 60 -> "$timeSinceStart second"
            timeSinceStart < 3600 -> "${timeSinceStart/60} minute"
            timeSinceStart < 86400 -> "${timeSinceStart/3600} hour"
            else -> "${timeSinceStart/86400} day"
        }.let { if (it.startsWith("1 ")) it else "${it}s" }.bold)
    }

    bot.command("!testGraph", "Check if graphing works", HelpType.Test) {
        displayPictureByUrl(repos.chartMaker.make("Test Graph", listOf(listOf(4, 50, 75, 20))))
    }

    bot.command("!addReply", "Add a new reply from the bot", HelpType.Simple) {
        when {
            "->" !in message -> reply("Usage: !addReply Hello bot. -> Hello person")
            message.split("->").size != 2 -> reply("You must use exactly 1 ->")
            else -> message.removePrefix("!addReply").split("->").also { (command, reply) ->
                bot.command(command.trim(), null, HelpType.NotTracked) { reply(reply.trim()) }
            }
        }
    }

    bot.command("!analyze", "Analyze text for keywords", HelpType.Query) {
        val result = repos.textAnalyzer.getKeywords(args.join()).reduce { x, y -> "$x, $y" }
        reply("The entities in your text are: $result")
    }

    bot.command("!help", "This command", HelpType.Simple) {
        val longestLength = help.flatMap { it.value.map { (name, _) -> name.length } }.max()!!
        reply("**~ ~ ~ ~The Following is best viewed on a wide screen~ ~ ~ ~**\n```diff\n" + help.flatMap { (helpType, map) ->
            listOf("- $helpType Commands") + map.map { (name, description) ->
                name + (" ".repeat(longestLength - name.length + 2)) + description
            } + listOf("\n")
        }.join("\n") + "\n- Extra Functionality\n" + extraHelp.map { (name, description) ->
            "$name\n\t$description"
        }.join("\n") + "```" )
    }

    bot.command("!genderCountryCodes", "Retrieve valid country codes for gender queries.", HelpType.Query) {
        repos.gender.countryStrings.forEach { reply(it) }
    }

    bot.command("!gender", "Discover your gender! (Only first name)", HelpType.Query) {
        reply(when {
            args.isEmpty() -> "You need to enter a name!"
            else -> repos.gender.get(args[0])
        })
    }

    bot.command("!genderFromCountry", "Discover your gender in a country! (Only first name)", HelpType.Query) {
        reply(when {
            args.size < 2 -> "Usage: !genderFromCountry Bill US"
            else -> repos.gender.get(args[0], args[1])
        })
    }

    bot.command("", null, HelpType.NotTracked) {
        checkForSteam(repos.steam)
        checkForDeletion(repos.userDatastore)
        checkForBibleVerses(repos.bible)
    }
}

private fun CommandEvent.checkForSteam(repo: SteamDatastore) {
    extraHelp["Steam Item Info"] = "Share a link to the steam store and get an app link + possible price comparisons!"
    if ("//store.steampowered.com/app/" in message) {
        val id = message.split("/")[4].toIntOrNull() ?: return
        val name = repo.getNameByID(id) ?: ""

        reply("steam://store/$id")?.appendMessage(repo.getSteamApp(name, id)?.run {
            "\nLowest: $$lowestPrice\nCurrent: $$price"
        } ?: "\nNo deal information found.")
    }
}

private fun CommandEvent.checkForDeletion(prefs: UserDatastore) {
    // extraHelp["Deletion Listener"] = "You can activate a preference to have your commands be deleted."
    if (message.startsWith("!") and prefs.getPreference(user, UserPreference.DeleteCommands))
        tryOrNull { deleteMessage() } ?: reply("Couldn't delete message, check permissions.")
}

private fun CommandEvent.checkForBibleVerses(bible: BibleDatastore) {
    extraHelp["Bible Verse"] = "Show bible verses."
    val args = message.split(":").join(" ").split(" ")

    if (args.size < 2)
        return

    val book = args[0]
    val chapter = args[1].toIntOrNull() ?: return
    val verse = args[2].toIntOrNull()
    val verses = args[2].split("-").mapNotNull { it.toIntOrNull() }

    when {
        verse != null -> bible.getVerse(book, chapter, verse)
        verses.isNotEmpty() -> bible.getVerses(book, chapter, verses[0], verses[1])?.join("\n")
        else -> ""
    }?.chunked(2000)?.forEach { reply(it) } ?: reply("Verse not found")
}

fun <T> Sequence<T>.takeWhileInclusive(pred: (T) -> Boolean): Sequence<T> {
    var shouldContinue = true
    return takeWhile {
        val result = shouldContinue
        shouldContinue = pred(it)
        result
    }
}
fun main() {
    val numbers = (0..1_000).map { (0..1_000).random() }
    println(measureTimeMillis { selectionSort(numbers) })
    println(measureTimeMillis { numbers.sort() })
    println(measureTimeMillis { quicksort(numbers) })
    // '2'.toString().toInt().let(::println)
    // listOf(1, 14, 4, 5, 7, 2, 3, 13).radixsort().let(::println)
}
val List<Int>.rs get() = map { it.toString().map { "$it".toInt() } }.let {
    (0..it.map { it.size }.max()!!).fold(it) { acc, i ->
        acc.sortedBy { it.getOrNull(i) }
    }.map { it.fold("") { acc, e -> "$acc$e" }.toInt() }
}

fun List<Int>.radixsort(): List<Int> {
    val digits = map { it.toString().map { "$it".toInt() } }
    val maxLength = digits.map { it.size }.max()!!

    return (0..maxLength).fold(digits) { acc, index ->
        acc.sortedBy { it.getOrNull(index) }
    }.map { it.fold("") { acc, e -> "$acc$e" }.toInt() }
}

fun <T: Comparable<T>> radixsort(li: Map<Int, T>): List<T> =
    li.entries.groupBy { it.key }.toSortedMap().values.flatMap { it.map { it.value } }


fun <T: Comparable<T>> quicksort(li: List<T>): List<T> {
    val pivot = li.firstOrNull() ?: return listOf()

    val left = quicksort(li.drop(1).filter { it < pivot })
    val right = quicksort(li.drop(1).filter { it >= pivot })

    return left + pivot + right
}

fun <T: Comparable<T>> selectionSort(li: List<T>, index: Int = 0): List<T> =
    li.drop(index).min()?.let { min -> listOf(min) + selectionSort(li.toMutableList().apply { remove(min) }) } ?: listOf()


fun <T: Comparable<T>> List<T>.sort(): List<T> {
    return fold(listOf()) { acc, e ->
        var found = false
        when {
            acc.isEmpty() -> listOf(e)
            e <= acc.min()!! -> listOf(e) + acc
            e >= acc.max()!! -> acc + listOf(e)
            else -> generateSequence(0 to listOf<T>()) { (i, _) ->
                val x = acc.getOrNull(i)
                val y = acc.getOrNull(i + 1)
                when {
                    x == null -> null
                    found -> listOf(x)
                    e >= x && y == null -> listOf(x, e).also { found = true }
                    e <= x && y == null -> listOf(e, x).also { found = true }
                    y != null && e in x..y -> listOf(x, e).also { found = true }
                    else -> listOf(x)
                }?.let { i + 1 to it }
            }.toMap().values.flatten().toList()
        }
    }
}

fun <T: Comparable<T>> qs(l: List<T>): List<T> = l.getOrNull(0)?.let { n ->
    qs(l.drop(1).filter { it < n }) + listOf(n) + qs(l.drop(1).filter { it >= n })
} ?: listOf()

val List<Int>.qs: List<Int> get() =
    getOrNull(0)?.let { n -> drop(1).filter { it < n }.qs + listOf(n) + drop(1).filter { it >= n }.qs } ?: listOf()
// val List<Int>.qs get() = [0]?.let(n -> filter(<n).qs + [n] + filter(>=n).qs) ?: []