package domain.commands

import domain.entities.DatastoreContainer
import domain.views.bots.HelpType
import domain.views.bots.QuoteBot

fun quoteCommands(bot: QuoteBot, repos: DatastoreContainer) {
    val repo = repos.userDatastore

    bot.command("!quoteAdd", "Adds a quote to the group datastore", HelpType.Local) {
        val quoteID = args[0].toIntOrNull()
        when  {
            args.size == 1 && quoteID != null -> {
                reply(channel?.let { channel ->
                    args[0].toLongOrNull()?.let { messageID ->
                        bot.getMessageById("$messageID", channel)?.let { msg ->
                            bot.getUserByIdOrName(msg.authorID)?.let { author ->
                                val quote = repo.addQuote(user, author, msg.content)
                                "Quote #${quote.id} added"
                            } ?: "Author ID inside message not valid"
                        } ?: "Message not found by ID"
                    } ?: "Message ID not a number"
                } ?: "Channel doesn't exist")
            }
            args.size < 2 -> reply("Usage: !quoteAdd <@mention_or_user_id> <text>\nor\nUsage: !quoteAdd <message_id>")
            else -> {
                val id = args[0].removePrefix("@")
                val author = bot.getUserByIdOrName(id) ?: mentionedUsers.getOrNull(0)
                if (author != null) {
                    val quote = repo.addQuote(user, author, args[1])
                    reply("Quote #${quote.id} added")
                }
                else
                    reply("Author not found, did you enter the correct user ID?")
            }
        }
    }

    bot.command("!quoteShow", "Shows a quote from the group datastore", HelpType.Local) {
        when (args.size) {
            0 -> reply("Usage: !quoteShow <id>")
            else -> {
                val quote = args[0].toIntOrNull()?.let { repo.getQuote(user, it) }
                println(quote?.value ?: "Quote not found")
                reply(quote?.value ?: "Quote not found")
            }
        }
    }
}