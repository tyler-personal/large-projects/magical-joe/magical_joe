package domain.commands

import domain.views.bots.SimpleBot
import domain.entities.DatastoreContainer
import domain.views.bots.HelpType
import domain.views.bots.extraHelp
import domain.views.events.CommandEvent
import io.github.vjames19.futures.jdk8.Future
import lib.join
import lib.tryOrNull
import lib.writeLine
import log
import java.io.*
import java.util.*
import java.util.concurrent.TimeUnit


private class Language(val name: String, val fileName: String, val command: String)
fun scriptCommands(bot: SimpleBot, repos: DatastoreContainer) {

    listOf(
        Language("Kotlin", "temp.kts", "kotlinc -script"),
        Language("Python", "temp.py", "python3"),
        Language("Ruby", "temp.rb", "ruby")
    ).forEach { lang ->
        fun CommandEvent.createLanguage() = newProcess(lang, args.join().removeSuffix("```"))

        extraHelp["Run Code"] = "Run code in Kotlin, Python, or Ruby by embedding your text inside a code block."
        bot.command("```${lang.name}\n", null, HelpType.NotTracked) { createLanguage() }
        bot.command("```${lang.name.toLowerCase()}\n", null, HelpType.NotTracked) { createLanguage() }
    }

    bot.command("!stop", "Stops all code processes", HelpType.Local) {
        processes.forEach(Process::destroy)
        processes.clear()
        reply("Processes destroyed")
    }
}

private val processes = mutableListOf<Process>()

private fun CommandEvent.newProcess(language: Language, text: String) {
    File(language.fileName).writeText(text)
    val pb = ProcessBuilder(language.command, language.fileName)
    val p1 = pb.start()
    processes.add(p1)


    val reader = Scanner(p1.inputStream)
    val writer = BufferedWriter(p1.outputStream.writer())
    writer.writeLine("Here they")
    while (true) {
        if (tryOrNull { Future { reader.hasNextLine() }.get(1, TimeUnit.SECONDS) } == true) {
            val line = reader.nextLine()
            reply(line)
        } else {
            val msg = channel?.getMessage() ?: user.privateMessage().message
            tryOrNull { Future { writer.write("$msg\n") }.get(1, TimeUnit.SECONDS) }
        }
    }
}

private fun <T> timedRun(milliseconds: Long, block: () -> T): T? =
    tryOrNull { Future { block() }.get(milliseconds, TimeUnit.MILLISECONDS) }

fun main() {
    val builder = ProcessBuilder("python3", "temp.py")
    val process = builder.start()

    val reader = process.inputStream.bufferedReader()
    val readBuffer = CharArray(1000)
    val writer = process.outputStream.bufferedWriter()

    while (true) {
        val charCount = timedRun(500) { reader.read(readBuffer) } ?: 0
        if (charCount > 0)
            println(String(readBuffer, 0, charCount))
        else {
            val line = readLine()!!
            writer.writeLine(line)
            writer.close()
        }
    }
}
// TODO: Reimplement the following
// val execute = "firejail --noprofile --overlay-tmpfs $command $fileName"
// val p1 = Runtime.getRuntime().exec(execute)
// val junk = "\u001B]0;firejail $command $fileName \u0007"
// val execute = "$command $fileName"