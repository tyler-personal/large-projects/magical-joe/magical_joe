package domain.commands

import domain.entities.DatastoreContainer
import domain.entities.PersistentThread
import domain.entities.enums.UserStatus
import domain.views.bots.plugins.VoicePlugin

fun voiceCommands(bot: VoicePlugin, repos: DatastoreContainer) {
    PersistentThread(seconds = 10) { checkForAfk(bot) }.start()
}

private fun checkForAfk(bot: VoicePlugin) {
    bot.servers
        .flatMap { server ->
            server.voiceChannels.map { it to server.afkChannel }
        }.mapNotNull { (channel, afk) ->
            val users = channel.connectedUsers
            users.getOrNull(0)?.takeIf { it.status == UserStatus.IDLE && users.size > 1 }?.let { it to afk }
        }.forEach { (user, afk) ->
            afk?.let { user.move(it) }
        }
}