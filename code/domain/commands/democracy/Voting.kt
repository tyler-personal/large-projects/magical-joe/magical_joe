package domain.commands.democracy

import domain.entities.DatastoreContainer
import domain.entities.enums.VotingChannelID
import domain.views.bots.HelpType
import domain.views.bots.VotingBot
import domain.views.channels.TextChannel
import lib.join

fun votingCommands(bot: VotingBot, repos: DatastoreContainer) {
    bot.command("!vote", "Vote for an issue!", HelpType.Local) {
        val votingChannel: TextChannel? = server?.let {
            repos.serverPrefs.getPreference(it, VotingChannelID)?.let(bot::getTextChannelById)
        } ?: channel

        when {
            args.isEmpty() -> reply("Usage: !vote voteName")
            votingChannel == null -> reply("You can't vote here! Either the voting channel isn't setup, or the bot doesn't support voting on this platform.")
            else -> votingChannel.sendMessage("Vote: ${args.join()}\nStarted by ${user.name}")
        }
    }
}

//fun <T> List<T>.append(t: T?) = t?.let { listOf(t) } ?: listOf()
//
//val <T> List<T>.head get() = firstOrNull() to subList(1, size)
//
//filter :: (t -> Bool) -> [t] -> [t]
//filter _ []     = []
//filter f (x:xs) = [x | f x] ++ filter f xs
//
//
//fun <T> List<T>.filter(f: (T) -> Boolean): List<T> = head.let { (x, xs) -> when {
//    x == null -> listOf()
//    else -> xs.filter(f).append(x)
//} }