package domain.commands

import domain.core.enums.UserPreference
import domain.views.bots.SimpleBot
import domain.entities.DatastoreContainer
import domain.views.bots.HelpType
import lib.join
import lib.spacesBeforeCapitals


fun preferenceCommands(bot: SimpleBot, repos: DatastoreContainer) {
    class PrefDetails(val value: UserPreference) {
        val name = value.name.capitalize().spacesBeforeCapitals()
        val lowerName = value.name.toLowerCase()

        override fun toString() = value.name.decapitalize()
    }

    val prefs = UserPreference.values().map(::PrefDetails)

    bot.command("!preferences", "Generate link for managing preferences.", HelpType.Local) {
        println(args)
        when (args.size) {
            0 -> {
                replyWithCode(prefs.map { "!preferences $it" }.join("\n") + "\n\n" + prefs.map { "!preferences $it <on/off>" }.join("\n"))
                replyWithNamedLink(title = "${user.name}'s Preferences", url = "${repos.siteUrl}/preferences/${user.id}")
            }
            1 -> prefs.find { it.lowerName == args[0].toLowerCase() }?.let { pref ->
                val onOrOff = if (repos.userDatastore.getPreference(user, pref.value)) "On" else "Off"
                reply("${pref.name}: $onOrOff")
            }
            2 -> prefs.find { it.lowerName == args[0].toLowerCase() }?.let { pref ->
                val onOrOff = args[1].toLowerCase()

                if (onOrOff !in listOf("on", "off"))
                    reply("Must supply \"on\" or \"off\".\nUsage: !p showImage on")
                else {
                    repos.userDatastore.setPreference(user, pref.value, onOrOff == "on")
                    reply("${pref.name} is now $onOrOff")
                }
            }
        }
    }
}
