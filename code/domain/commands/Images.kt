package domain.commands

import domain.core.enums.UserPreference
import domain.datastores.ImageDatastore
import domain.views.bots.SimpleBot
import domain.entities.DatastoreContainer
import domain.entities.structs.ImageAnnotations
import domain.views.bots.HelpType
import domain.views.bots.extraHelp
import domain.views.events.ImageCommandEvent
import lib.join

fun imageCommands(bot: SimpleBot, repos: DatastoreContainer) {
    extraHelp["Image Listener"] = "Give information about shared images. To enable: `!preferences showImage on`"
    bot.imageCommand("!image", "Manually invoke the image listener. Run: `!preferences showImage on` to run this command automatically", HelpType.Query) {
        displayAnnotations(repos.imageDatastore)
    }

    bot.imageCommand("", null, HelpType.NotTracked) {
        if (!repos.userDatastore.getPreference(user, UserPreference.ShowImage))
            return@imageCommand
        displayAnnotations(repos.imageDatastore)
    }
}

private fun ImageCommandEvent.displayAnnotations(datastore: ImageDatastore) {
    fun header(data: String) = "~ ~ ~ ~ ~ ${data.italics.bold} ~ ~ ~ ~ ~"
    val result = datastore.generateFromBytes(messageAsBytes)

    if (result == null) {
        reply("Accessing ${datastore.name} failed.")
        reply("If you want to disable this feature, do `!preferences showImage off`")
        return
    }
    reply("""
        ${header("I see")}
        ${if (result.labels != null) output(result.labels) else "Nothing"}
        
        ${header("I think this may be")}
        ${if (result.web != null) output(result.web) else "I don't know what this is."}
        
        ${header("Sources")}
        ${result.urlsWithImage?.join("\n") { it.hideURL } ?: "No websites found"}
        
        ${header("Similar Images")}
        ${result.similarImageURLs?.join("\n") { it.hideURL } ?: "No similar images found"}
    """.trimIndent())
}

private fun output(li: List<ImageAnnotations.Result>, separator: String = "\n", count: Int = 3) =
    li.asSequence()
        .filter { it.description != null && !it.description.isBlank() }
        .take(count)
        .joinToString(separator) { "${it.description} (${it.score?.format(3)})" }

private fun Double.format(digits: Int) = java.lang.String.format("%.${digits}f", this)
