package domain.commands.queries

import domain.entities.DatastoreContainer
import domain.entities.structs.movies.ActorQuery
import domain.entities.structs.movies.MovieQuery
import domain.views.bots.HelpType
import domain.views.bots.SimpleBot
import domain.views.events.CommandEvent
import lib.join

fun movieCommands(bot: SimpleBot, repos: DatastoreContainer) {
    fun <Query : Any, Result : Any> CommandEvent.search(
        type: Pair<String, String>, usage: String, command: String,
        queryConstructor: (String) -> Query, findQueryResult: (Query) -> Result?,
        findFinalResult: (List<Result>) -> List<String>) {
        val msg = message.removePrefix(command).trim()
        reply(when {
            msg.isEmpty() -> "Usage: $command $usage"
            else -> {
                val queries = msg.split("&").filter { it.isNotEmpty() }.map { queryConstructor(it.trim()) }
                val results = queries.map { it to findQueryResult(it) }
                val failedQueries = results.filter { it.second == null }

                if (failedQueries.isNotEmpty())
                    "The following ${type.first} were not found:\n${failedQueries.map { "${it.first}" }.join("\n")}"
                else {
                    val result = findFinalResult(results.map { it.second!! })
                    when (result.size) {
                        0 -> "Found no ${type.second} for the ${type.first}"
                        else -> result.join("\n")
                    }
                }
            }
        })
    }

    val repo = repos.actor
    println("We're in movies")

    bot.command("!actorSearch", "Search for actors by movies they were in.", HelpType.Query) {
        search("movies" to "actors", "Movie1 & Movie2 & Movie3 etc.", "!actorSearch",
            ::MovieQuery, repo::findMovie, repo::findSameActors)
    }

    bot.command("!movieSearch", "Search for movies by the actors that were in them.", HelpType.Query) {
        println("Movie Search fired!")
        search("actors" to "movies", "Actor1 & Actor2 & Actor3 etc.", "!movieSearch",
            ::ActorQuery, repo::findActor, repo::findSameMovies)
    }
}
