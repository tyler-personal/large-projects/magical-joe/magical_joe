package domain.commands

import domain.boundaries.datastores.BibleDatastore
import domain.entities.DatastoreContainer
import domain.views.bots.HelpType
import domain.views.bots.QuoteBot
import domain.views.bots.plugins.FindUserPlugin

fun userSpecificCommand(bot: QuoteBot, repos: DatastoreContainer) {
    val bible = repos.bible
    val garrett = "106973447707455488"
    val cole = "428157808458989578"
    bot.command("", null, HelpType.NotTracked) {
        if (server?.let(user::getNickname)?.contains("Tyler Brown") == true) {
            val (verse, text) = bible.getRandomVerse()
            reply("${verse.bold}: $text")
        }
    }
}
