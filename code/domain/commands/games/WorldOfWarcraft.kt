package domain.commands.games

import domain.datastores.games.WoWAuctionDatastore
import domain.views.bots.SimpleBot
import domain.entities.DatastoreContainer
import domain.views.events.CommandEvent
import domain.entities.structs.games.WoWAuctionItem
import domain.entities.structs.games.WoWCoins
import domain.interactors.EpochToDateConverter
import domain.views.bots.HelpType
import lib.*
import log

fun wowCommands(bot: SimpleBot, repos: DatastoreContainer) {
    fun serverExists(server: String) = repos.wowAuction.remote.exists(server)
    fun String.normalize() = split("-").join().capitalizeWords()

    val repo = repos.wowAuction

    bot.command("!wowAuction", "Search for WoW items on the auction house", HelpType.Game) {
        val (server, item) = when {
            args.isEmpty() -> {
                reply("Usage: !wowAuction <optional-server> <item name>")
                return@command
            }
            args.size > 1 && serverExists(args[0]) ->
                args[0] to args.drop(1).join()
            args.size > 2 && serverExists("${args[0]}-${args[1]}") ->
                "${args[0]}-${args[1]}" to args.drop(2).join()
            else ->
                "Bleeding-Hollow" to args.join()
        }.apply { first.normalize() to second.normalize() }

        log("!wowAuction fired with $server $item")

        if (!repo.upToDate(server))
            refreshServer(server, repo)

        val auction = repo.local.getItem(repos.wowItems, item, server)
        val coins = auction?.minCost

        if (coins != null) {
            val lastUpdated = EpochToDateConverter.perform(repo.local.lastModified(server) ?: 0.toBigInteger())
            val result = "$item - $server Statistics".bold + "\n" +
                "[Lowest Buyout] $coins\n" +
                "[10% Median] ${auction.bottom10Median}\n" +
                "[25% Median] ${auction.bottom25Median}\n" +
                "[Volume] ${auction.count}\n" +
                "[Last Updated] " + lastUpdated.codeSnip
            replyWithCode(result)
        } else {
            val suggestions = repo.local.getNameSuggestions(item, server)?.take(3)
            reply("Item not found on the auction house." + when {
                suggestions?.isNotEmpty() == true -> "\nSuggestions: " + suggestions.join(", ")
                else -> ""
            })
        }

        repo.stats.getItemStatistics(item, server)?.let { items ->
            fun showChart(title: String, getCoins: (WoWAuctionItem) -> WoWCoins?, color: String) {
                val coins = items.mapNotNull { getCoins.invoke(it) }
                val type = coins.maxBy { it.total }?.mainCoin ?: return
                val values = coins.map { it.getTotalByType(type) }
                val str = "$title: ${type.name.capitalize()}"
                displayPictureByUrl(repos.chartMaker.make(str, listOf(values), colors=listOf(color)))
            }

            showChart("Lowest Buyout", WoWAuctionItem::minCost, "FACA7A")
            showChart("10% Median", WoWAuctionItem::bottom10Median, "990000")
            showChart("25% Median", WoWAuctionItem::bottom25Median, "660000")
        }
    }

    bot.command("!wowAuctionRefresh", "Refresh auction houses by server", HelpType.Game) {
        refreshServer(when (args.size) {
            0 -> {
                reply("Using default server: Bleeding-Hollow")
                "Bleeding-Hollow"
            }
            else -> args[0]
        }, repo)
    }

    bot.command("!wowCharacter", "Search for a character's item level", HelpType.Game) {
        reply(when (args.size) {
            0 -> {
                reply("Usage: !wowCharacter <optional-server> <character-name>")
                return@command
            }
            1 -> {
                reply("Using default server: Bleeding-Hollow")
                "Bleeding-Hollow" to args[0]
            }
            2 -> args[0] to args[1]
            3 -> "${args[0]}-${args[1]}" to args[2]
            else -> {
                reply("Usage: !wowCharacter <optional-server> <character-name>")
                reply("Remember WoW character names can't have spaces in them.")
                return@command
            }
        }.run { repos.wowCharacters.get(second, first) }?.run {
            "Item Level: $itemLevel"
        } ?: "Character not found")
    }
}

private fun CommandEvent.refreshServer(server: String, repo: WoWAuctionDatastore) {
    reply("Refreshing WoW Auction Data on $server".bold)
    reply("If local is not up to date after refresh, Blizzard's API might not have refreshed".italics)
    repo.refresh(server)
    reply("Refresh complete!".bold)
}

