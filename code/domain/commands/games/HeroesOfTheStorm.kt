package domain.commands.games

import domain.entities.DatastoreContainer
import domain.entities.structs.games.heroes.Hero
import domain.interactors.StringSearcher
import domain.views.bots.HelpType
import domain.views.bots.SimpleBot
import lib.*

fun heroesCommands(bot: SimpleBot, repos: DatastoreContainer) {
    val repo = repos.heroes

    bot.command("!hots game", "Gives pick suggestions based on a partially filled team.", HelpType.Game) {
        val (withInputs, againstInputs) = listOf("game [" to "] vs", "vs [" to "]").map { (before, after) ->
            tryOrNull { message.between(before, after) }?.split(",")?.map { it.trim() }
        }

        if (args.isEmpty() || withInputs == null || againstInputs == null) {
            reply("Usage: !hots game [heroName1, heroName2, heroName3] vs  [heroName1, heroName2, heroName3]")
            return@command
        }

        val (withHeroes, againstHeroes) = listOf(withInputs, againstInputs).map {
            it.map { name -> StringSearcher.perform(name, repo.heroesNames) }.map(repo::getHeroInfo)
        }

        val suggestions = mutableMapOf<Hero, List<String>>()

        fun suggestionProcessing(heroes: List<Hero>, type: (Hero) -> List<Hero>, reason: (Hero) -> String) =
            heroes.forEach { hero ->
                type(hero).forEach { foundHero ->
                    val r = reason(hero)
                    suggestions[foundHero] = suggestions[foundHero]?.let { it + r } ?: listOf(r)
                }
            }

        suggestionProcessing(withHeroes, Hero::synergies) { "Synergizes with $it" }
        suggestionProcessing(againstHeroes, Hero::counters) { "Counters $it" }

        val currentRoles = withHeroes.map { it.role }
        suggestions
            .filter { (hero, _) ->
                hero.role !in currentRoles && hero.name !in withInputs && hero.name !in againstInputs
            }.forEach { (hero, reasons) ->
                suggestions[hero] = reasons + listOf("Fills currently empty role of ${hero.role}")
            }

        val maxCount = suggestions.maxBy { it.value.size }?.value?.size ?: 0
        println(maxCount)
        val picks = maxCount.downTo(0)
            .flatMap { num -> suggestions.filter { it.value.size == num }.toList() }.take(5).toMap()

        val displaySuggestions =
            picks.map { (hero, reasons) -> "\n$hero\n" + reasons.join("\n") { "\t$it" } }.toString().between("[", "]")
        reply("Best picks: \n$displaySuggestions")
//        reply("""
//${"Synergies".bold}
//${synergies.map { (str, count) -> "$str ($count)" }.join("\n")}
//${"Counters".bold}
//${counters.map { (str, count) -> "$str ($count)" }.join("\n") }
//        """.trimIndent())
//    }
    }
}

fun main() {
    "YAAT".between("YA", "T").let(::println)
}