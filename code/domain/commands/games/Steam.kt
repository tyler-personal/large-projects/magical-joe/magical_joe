package domain.commands.games

import domain.views.bots.SimpleBot
import domain.entities.DatastoreContainer
import domain.views.bots.HelpType
import lib.join

fun steamCommands(bot: SimpleBot, repos: DatastoreContainer) {
    val gamesByServerID = mutableMapOf<String, String>()

    bot.command("", null, HelpType.NotTracked) {
        repos.steam.getAllSteamApps().forEach { steamApp ->
            if (steamApp.name.toLowerCase() in message.toLowerCase()) {
                val parts = steamApp.name.split(" ")

                val inside = parts.all { part ->
                    message.split(" ").any {
                        when {
                            parts.size > 1 || it.length > 4 -> it.equals(part, true)
                            else -> it == part
                        }
                    }
                }

                if (inside)
                    server?.let { gamesByServerID[it.id] = "$steamApp" }

            }

        }
    }

    bot.command("!mentionedGames", "See all games you've mentioned since you last ran this command.", HelpType.Game) {
        reply(gamesByServerID.values.toList().join("\n"))
        gamesByServerID.clear()
    }
}