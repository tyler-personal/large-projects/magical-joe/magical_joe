package domain.commands.games

import domain.views.bots.SimpleBot
import domain.entities.DatastoreContainer
import domain.entities.structs.games.albion.AlbionItem
import domain.entities.structs.games.albion.AlbionLocation
import domain.views.bots.HelpType
import lib.*

private class OutputLine(location: AlbionLocation, price: AlbionItem.Price) {
    val location = location.toString().toLowerCase().replace("_", " ").capitalizeWords()
    val price = "${formatPrice(price.sellMin)} to ${formatPrice(price.sellMax)}"

    private fun formatPrice(price: Int) = when {
        price > 10_000_000 -> "${price / 1_000_000}m"
        price > 10_000 -> "${price / 1_000}k"
        else -> "$price"
    }
}

fun albionCommands(bot: SimpleBot, repos: DatastoreContainer) {
    bot.command("!albionPrices", "Show price for an item in Albion Online", HelpType.Game) {
        if (args.isEmpty()) {
            reply("Usage: !albionPrices <item_name>")
            return@command
        }

        val item = repos.albion.getItem(args.join(), user)
        val prices = AlbionLocation.values()
            .mapNotNull { location -> item?.getPrice(location)?.let { location to it } }
            .map { (x, y) -> OutputLine(x, y) }

        val (startingZonePrices, endZonePrices) = prices.splitAtLast { it.location.contains("Cross") }

        val longestLocation = prices.map { it.location.length }.max()!!
        val longestPrice = prices.map { it.price.length }.max()!!

        fun OutputLine.zoneFormat() =
            "$location: ${" ".repeat(longestLocation - location.length)}" +
            "$price ${" ".repeat(longestPrice - price.length)} "

        fun List<OutputLine>.show(label: String, spaceCount: Int) {
            reply(" ".repeat(spaceCount) + label.italics)
            replyWithCode(when {
                isNotEmpty() -> joinToString("\n") { it.zoneFormat() }
                else -> "No Data Available"
            })
        }

        reply(item.toString().capitalizeWords().bold + "\n\n")
        startingZonePrices.show("Starting Zones", 25)
        endZonePrices.show("Late Zones", 28)
    }
}