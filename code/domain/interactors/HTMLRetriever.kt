package domain.interactors

import org.jsoup.Jsoup

object HTMLRetriever {
    fun perform(url: String) = Jsoup.connect(url).get()

}