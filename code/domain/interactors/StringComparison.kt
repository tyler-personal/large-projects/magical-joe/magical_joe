package domain.interactors

import domain.core.enums.UserPreference
import domain.datastores.UserDatastore
import domain.views.users.User
import info.debatty.java.stringsimilarity.*

object StringComparison {
    private val defaultComparator = Levenshtein()
    private val normalizedComparator = NormalizedLevenshtein()

    fun perform(first: String, second: String, userPrefs: UserDatastore? = null, user: User? = null) =
        (user?.let {
            if (userPrefs?.getPreference(it, UserPreference.NormalizedSearch) == true)
                normalizedComparator
            else
                defaultComparator
        } ?: defaultComparator).distance(first, second)
}