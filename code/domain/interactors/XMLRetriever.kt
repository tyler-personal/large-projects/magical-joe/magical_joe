package domain.interactors

import org.w3c.dom.Document
import javax.xml.parsers.DocumentBuilderFactory

// TODO remove XML dependency from domain layer
object XMLRetriever {
    private val builder = DocumentBuilderFactory.newInstance().newDocumentBuilder()

    fun perform(url: String): Document = builder.parse(url)
}