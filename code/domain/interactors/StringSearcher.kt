package domain.interactors

import domain.datastores.UserDatastore
import domain.views.users.User
import log

object StringSearcher {
    fun perform(x: String, xs: Collection<String>, userPrefs: UserDatastore? = null, user: User? = null) = xs.also {
        log(x)
        log("$xs")
    }.map {
        StringComparison.perform(x, it, userPrefs, user) to it
    }.minBy { it.first }?.second!!
}

