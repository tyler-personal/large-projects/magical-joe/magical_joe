package domain.interactors

import java.math.BigInteger
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.TimeUnit


object EpochToDateConverter {
    val simpleFormat = "MM/dd/yy"
    val advancedFormat = "MMMM dd hh:mm a"

    fun perform(epoch: BigInteger, pattern: String = advancedFormat): String {
        val format = DateTimeFormatter.ofPattern(pattern)
        return LocalDateTime.ofEpochSecond(epoch.toLong() / 1000, 0, ZoneOffset.ofHours(-4)).format(format)
    }
}