package domain.interactors

import java.net.URL

object ImageBytesFromURL {
    fun perform(url: String): ByteArray {
        return URL(url).readBytes()
    }
}