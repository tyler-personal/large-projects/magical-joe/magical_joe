package domain.interactors

import java.math.BigInteger
import java.util.*
import java.util.concurrent.TimeUnit



object EpochDateDistanceConverter {
    fun perform(oldEpoch: BigInteger, newEpoch: BigInteger, timeUnit: TimeUnit = TimeUnit.DAYS): Long {
        val old = Date(oldEpoch.toLong()).time
        val new = Date(newEpoch.toLong()).time

        return timeUnit.convert(old - new, TimeUnit.MILLISECONDS)
    }
}