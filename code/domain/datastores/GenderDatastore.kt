package domain.datastores

interface GenderDatastore {
    val countryStrings: List<String>
    fun get(name: String, countryCode: String? = null): String
}