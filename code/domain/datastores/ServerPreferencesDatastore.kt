package domain.datastores

import domain.entities.enums.ServerPreference
import domain.views.servers.Server

interface ServerPreferencesDatastore {
    fun <T: Any, Pref: ServerPreference<T>> getPreference(server: Server, preference: Pref): T?
    fun <T: Any, Pref: ServerPreference<T>> setPreference(server: Server, preference: Pref, value: T)
}