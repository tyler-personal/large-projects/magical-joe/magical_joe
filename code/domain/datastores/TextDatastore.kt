package domain.boundaries.datastores

interface TextDatastore {
    fun getKeywords(text: String): List<String>
}