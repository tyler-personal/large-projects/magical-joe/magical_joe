package domain.datastores

import domain.entities.structs.movies.*

interface ActorDatastore {
    fun findMovieByID(movie: Int): String?
    fun findActorByID(actor: Int): String?

    fun findMovie(movie: MovieQuery): MovieResult?
    fun findActor(actor: ActorQuery): ActorResult?

    fun findSameMovies(actors: List<ActorResult>): List<String> =
        findSameResults(actors, ActorResult::movieIDs, ::findMovieByID)

    fun findSameActors(movies: List<MovieResult>): List<String> =
        findSameResults(movies, MovieResult::actorIDs, ::findActorByID)

    private fun <R> findSameResults(results: List<R>, getIDs: (R) -> List<Int>, findByID: (Int) -> String?) =
        results.map(getIDs).reduce { x, y -> x.intersect(y).toList() }.mapNotNull(findByID)
}