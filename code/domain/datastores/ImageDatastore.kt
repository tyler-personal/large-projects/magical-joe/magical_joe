package domain.datastores

import domain.entities.structs.ImageAnnotations

interface ImageDatastore {
    val name: String
    fun generateFromBytes(bytes: ByteArray): ImageAnnotations?
}