package domain.datastores.games

import domain.entities.structs.games.albion.AlbionItem
import domain.views.users.User

interface AlbionDatastore {
    fun getItem(name: String, user: User? = null): AlbionItem?
}