package domain.datastores.games

import domain.entities.structs.SimpleSteamApp
import domain.entities.structs.SteamApp

interface SteamDatastore {
    fun getNameByID(id: Int): String?
    fun getSteamApp(name: String, id: Int): SteamApp?
    fun getAllSteamApps(): List<SimpleSteamApp>
}