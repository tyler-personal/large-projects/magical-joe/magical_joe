package domain.datastores.games

interface WoWItemDatastore {
    fun getItemName(id: Int): String?
    fun getItemID(name: String): Int?
}