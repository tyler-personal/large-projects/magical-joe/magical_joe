package domain.datastores.games

import domain.entities.structs.games.heroes.Hero

interface HeroesDatastore {
    fun getHeroInfo(inputName: String): Hero
    val heroesNames: List<String>
}