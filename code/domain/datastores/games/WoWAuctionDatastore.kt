package domain.datastores.games

import domain.entities.structs.games.WoWAuctionItem
import domain.entities.structs.games.WoWAuctionServer
import java.math.BigInteger

interface WoWAuctionDatastore {
    val local: Local
    val remote: Remote
    val stats: Statistics

    fun upToDate(server: String): Boolean = local.lastModified(server) == remote.lastModified(server)
    fun refresh(server: String) {
        local.updateServer(remote, server)
    }

    interface Base {
        fun lastModified(server: String): BigInteger?
        fun exists(server: String): Boolean = lastModified(server) != null
    }

    interface Remote: Base {
        fun getAuctionServer(server: String): WoWAuctionServer?

        fun versionOutOfDate(server: String, retrieverRepo: Local): Boolean =
            retrieverRepo.lastModified(server) != lastModified(server)
    }

    interface Local: Base {
        fun getNameSuggestions(name: String, server: String): List<String>?
        fun getItem(id: Int, server: String): WoWAuctionItem?
        fun updateServer(remote: Remote, server: String): Boolean

        fun getItem(repo: WoWItemDatastore, name: String, server: String): WoWAuctionItem? {
            return repo.getItemID(name)?.let { getItem(it, server) }
        }
    }

    interface Statistics: Base {
        fun getItemStatistics(name: String, server: String): List<WoWAuctionItem>?
    }
}