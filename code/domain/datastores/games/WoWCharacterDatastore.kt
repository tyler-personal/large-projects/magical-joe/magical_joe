package domain.datastores.games

import domain.entities.structs.games.WoWCharacter

interface WoWCharacterDatastore {
    fun get(name: String, server: String): WoWCharacter?
}