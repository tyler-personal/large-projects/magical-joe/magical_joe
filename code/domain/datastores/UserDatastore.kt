package domain.datastores

import domain.core.enums.UserPreference
import domain.entities.structs.Quote
import domain.views.users.User

interface UserDatastore {
    fun getPreference(user: User, pref: UserPreference): Boolean
    fun setPreference(user: User, pref: UserPreference, value: Boolean)

    fun addQuote(user: User, author: User, quote: String): Quote
    fun getQuotes(user: User): List<Quote>
    fun editQuote(user: User, quoteId: Int, quote: String): Boolean
    fun deleteQuote(user: User, quoteId: Int): Boolean

    fun getQuote(user: User, id: Int) = getQuotes(user).find { it.id == id }
}