package domain.datastores

import domain.entities.structs.Poll

interface PollDatastore {
    fun createPoll(name: String): Poll
}