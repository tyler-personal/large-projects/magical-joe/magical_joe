package domain.boundaries.datastores


interface BibleDatastore {
    fun getVerse(book: String, chapter: Int, verse: Int): String?
    fun getRandomVerse(): Pair<String, String>
    fun getVerses(book: String, chapter: Int, start: Int, end: Int): List<String>?
}