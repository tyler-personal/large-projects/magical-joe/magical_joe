package domain.boundaries.datastores.images

interface ChartDatastore {
    fun make(
        name: String,
        values: List<List<Int>>,
        colors: List<String> = listOf("FACA7A", "990000", "660000")
    ): String
}