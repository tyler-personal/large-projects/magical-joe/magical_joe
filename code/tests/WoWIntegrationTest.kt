import data.local.databases.BlizzardDB
import data.local.json.WoWStatisticsJSON
import data.local.memory.WoWAuctionMemoryStore
import data.remote.blizzard.WoWAuctionAPI
import data.remote.blizzard.WoWItemAPI
import domain.datastores.games.WoWAuctionDatastore
import domain.datastores.games.WoWItemDatastore
import org.junit.Test

class WoWIntegrationTest {
    @Test fun execute() {
        val wowItems: WoWItemDatastore = WoWItemAPI()
        val wowAuction = object : WoWAuctionDatastore {
            override val local: WoWAuctionDatastore.Local = WoWAuctionMemoryStore()
            override val remote: WoWAuctionDatastore.Remote = WoWAuctionAPI()
            override val stats: WoWAuctionDatastore.Statistics = WoWStatisticsJSON(remote, wowItems)
        }

        wowAuction.refresh("Bleeding-Hollow")

        fun check(name: String) {
//            val result = WoWAHSearcher.perform(name)?.minCost
//            val bZero = 0.toBigInteger()
//            assert(result != null && result.silver > 1.toBigInteger())
//            assert(result != null && (result.copper > bZero || result.silver > bZero || result.gold > bZero))
        }

        listOf("Linen Cloth", "Deep Sea Satin").forEach(::check)
    }
}