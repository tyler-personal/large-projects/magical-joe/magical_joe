package tests.data
import data.remote.google.GoogleImageAnnotator
import org.junit.Test
import java.io.File


class GoogleImageAnnotatorTest {
    @Test fun generateFromBytes() {
        val bytes = File("code/tests/test.jpg").readBytes()
        val result = GoogleImageAnnotator().generateFromBytes(bytes)
    }
}