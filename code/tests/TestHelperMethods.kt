//import domain.boundaries.repositories.worldofwarcraft.WoWCharacterDatastore
//import domain.boundaries.views.*
//import domain.core.entities.ListenerManager
//import domain.core.entities.worldofwarcraft.WoWCharacter
//import domain.core.interactors.ArgumentParser
//
//private var viewListeners = mutableListOf<Listener>()
//var testInput: String = ""
//    set(value) {
//        viewListeners.forEach { viewListener ->
//            val args = ArgumentParser.perform(viewListener.command, value, viewListener.separator)
//
//            if (args != null) {
//                val commandMessage = object : CommandMessage {
//                    override val args: List<String> = args
//                    override val content: String = value
//                    override val fromUser: Boolean = true
//                }
//
//                val commandOutput = object : CommandOutput {
//                    override fun send(message: String) {
//                        testOutput += message
//                    }
//
//                    override fun displayPictureByURL(url: String) {
//                        testOutput += url
//                    }
//                }
//                viewListener.block(commandMessage, commandOutput)
//            }
//        }
//    }
//
//var testOutput: String = ""
//
//private class Listener(val command: String, val separator: String, val block: CommandBlock)
//
//private fun instantiateTestViewContainers() {
//    ViewContainer.commandsView = object : CommandsView {
//        override fun addMessageListener(command: String, separator: String, showToUser: Boolean, block: CommandBlock) {
//            viewListeners.add(Listener(command, separator, block))
//        }
//    }
//
//
//}
//
//private fun instantiateTestRepoContainers() {
//    RepoContainer.wowCharacterRepo = object : WoWCharacterDatastore {
//        override fun get(name: String, server: String): WoWCharacter? {
//            return if (name == "Teneros" && server == "Bleeding-Hollow")
//                WoWCharacter(name, 340)
//            else null
//        }
//    }
//}
//
//fun startTest(useTestRepos: Boolean=true) {
//    instantiateTestViewContainers()
//    if (useTestRepos)
//        instantiateTestRepoContainers()
//    else
//        instantiateRepoContainers()
//    CommandManager.initializeCommandsAndListeners()
//}