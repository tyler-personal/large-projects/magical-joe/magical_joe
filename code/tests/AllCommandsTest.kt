//import org.junit.Test
//
//class AllCommandsTest {
//    @Test fun wowCharacterCommand() {
//        startTest()
//
//        testInput = "!wowCharacter Teneros Bleeding-Hollow"
//        assert(testOutput.contains("Item Level: 340"))
//        testOutput = ""
//
//        testInput = "!wowCharacter TEEE bb"
//        assert(!testOutput.contains("Item Level"))
//    }
//
//    @Test fun wowCharacterDefaultCommand() {
//        startTest()
//
//        testInput = "!wowCharacter Teneros"
//        assert(testOutput.contains("Using default server"))
//        assert(testOutput.contains("Item Level: 340"))
//        testOutput = ""
//
//        testInput = "!wowCharacter potato"
//        assert(testOutput.contains("Using default server"))
//        assert(!testOutput.contains("Item level: 340"))
//    }
//
//    @Test fun wowAuctionCommand() {
//        startTest(useTestRepos = false)
//
//        testInput = "!wowAuction Linen Cloth"
//
//        assert(testOutput.contains("Lowest Buyout"))
//        assert(!testOutput.contains("0G 0S 0C"))
//    }
//
//    @Test fun wowAuctionRefreshCommand() { } // TODO: Implement
//}