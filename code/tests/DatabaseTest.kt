import data.local.databases.Database
import org.junit.Test
import kotlin.system.measureTimeMillis

class DatabaseTest {
    @Test fun perform() {
        val db = Database(databaseName = "magicaljoetest")

        db.executeSQL("DROP TABLE users;")
        db.executeSQL("CREATE TABLE users(name varchar(255), age int)")

        val users = mapOf("Tyler" to 21, "Potato" to 34, "Argh" to 49, "HEYHEY" to 38)
        users.forEach { name, age -> db.executeSQL("INSERT INTO users (name, age) VALUES ('$name', $age)") }

        var result = listOf<String>()

        println("QUERY & GET_STRING TIME: " + measureTimeMillis {
            result = db.executeQuery("SELECT * FROM users") {
                it.getString("name")
            }
        } + " ms")

        listOf("Tyler", "Potato", "Argh", "HEYHEY").forEach {
            assert(result.contains(it))
        }
    }

}