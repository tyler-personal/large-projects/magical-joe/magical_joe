package data.local.memory

import domain.datastores.games.WoWAuctionDatastore
import domain.entities.structs.games.WoWAuctionItem
import domain.entities.structs.games.WoWAuctionServer
import java.math.BigInteger

class WoWAuctionMemoryStore: WoWAuctionDatastore.Local {
    private val auctionServerByName = mutableMapOf<String, WoWAuctionServer>()

    override fun getNameSuggestions(name: String, server: String): List<String>? =
        auctionServerByName[server]?.itemNames?.filter { it.contains(name) }

    override fun getItem(id: Int, server: String): WoWAuctionItem? =
        auctionServerByName[server]?.itemsByID?.get(id)

    override fun lastModified(server: String): BigInteger? =
        auctionServerByName[server]?.lastUpdatedEpoch

    override fun updateServer(remote: WoWAuctionDatastore.Remote, server: String): Boolean {
        auctionServerByName[server] = remote.getAuctionServer(server) ?: return false
        return true
    }
}