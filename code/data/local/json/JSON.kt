package data.local.json

import domain.views.users.User
import org.json.JSONObject
import java.io.File

open class JSON(fileName: String) {
    protected val file = File("data/").let { dirs ->
        if (!dirs.exists())
            dirs.mkdirs()
        File("data/$fileName").also { file ->
            if (!file.exists()) {
                file.createNewFile()
                file.writeText("{}")
            }
        }
    }
    protected val json = JSONObject(file.readText())

    protected fun write(objectID: String, propertyID: String, value: Any) {
        val jsonObject = json.optJSONObject(objectID) ?: JSONObject()

        jsonObject.put(propertyID, value)
        json.put(objectID, jsonObject)

        file.writeText(json.toString())
    }

    protected fun find(user: User): JSONObject? = json.optJSONObject(user.id)
}