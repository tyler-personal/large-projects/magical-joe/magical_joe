package data.local.json

import domain.datastores.ServerPreferencesDatastore
import domain.entities.enums.ServerPreference
import domain.views.servers.Server
import kotlin.reflect.full.safeCast
import kotlin.reflect.jvm.jvmName

class ServerPreferencesJSON : ServerPreferencesDatastore, JSON("server_preferences.json") {
    override fun <T: Any, Pref : ServerPreference<T>> getPreference(server: Server, preference: Pref): T? {
        return json.optJSONObject(server.id)?.opt(preference.type.simpleName)?.let {
            preference.type::class.safeCast(it)?.objectInstance
        }
    }

    override fun <T: Any, Pref : ServerPreference<T>> setPreference(server: Server, preference: Pref, value: T) {
        write(server.id, preference.type.simpleName ?: preference.type.jvmName , value)
    }
}