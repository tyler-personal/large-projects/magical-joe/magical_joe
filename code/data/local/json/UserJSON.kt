package data.local.json

import domain.core.enums.UserPreference
import domain.datastores.UserDatastore
import domain.entities.structs.Quote
import domain.views.users.User
import lib.join
import org.json.JSONObject

class UserJSON : UserDatastore, JSON("user_preferences.json") {
    override fun getPreference(user: User, pref: UserPreference): Boolean {
        return find(user)?.optBoolean(pref.name) ?: false
    }

    override fun setPreference(user: User, pref: UserPreference, value: Boolean) {
        write(user.id, pref.name, value)
    }

    override fun addQuote(user: User, author: User, quote: String): Quote {
        val id = getLastQuoteId(user) + 1
        write("quotes", "$id", "${user.id}_${author.id}_$quote")
        return Quote(id, quote, user.id, author.id)
    }

    override fun getQuotes(user: User): List<Quote> {
        val quotesJson = json.optJSONObject("quotes") ?: JSONObject()
        return quotesJson.keySet()
            .map { id ->
                val (userID, authorID, quote) = quotesJson.getString(id).split("_").let {
                    listOf(it[0], it[1], it.subList(2, it.size).join("_"))
                }
                Quote(id.toInt(), quote, userID, authorID)
            }
    }

    override fun editQuote(user: User, quoteID: Int, newQuote: String): Boolean {
        val quote = getQuotes(user).find { it.id == quoteID } ?: return false
        write("quotes", "$quoteID", "${quote.adderUserID}_${quote.authorUserID}_$newQuote")
        return true
    }

    override fun deleteQuote(user: User, quoteId: Int): Boolean {
        return editQuote(user, quoteId, "")
    }

    private fun getLastQuoteId(user: User): Int {
        return getQuotes(user).lastOrNull()?.id ?: 0
    }

}