package data.local.json

import com.google.gson.Gson
import domain.datastores.games.WoWAuctionDatastore
import domain.datastores.games.WoWItemDatastore
import domain.entities.PersistentThread
import domain.entities.structs.games.WoWAuctionItem
import domain.entities.structs.games.WoWAuctionServer

class WoWStatisticsJSON(
    remote: WoWAuctionDatastore.Remote, private val itemRepo: WoWItemDatastore
): WoWAuctionDatastore.Statistics, JSON("wow_stats.json") {

    init {
        PersistentThread(minutes = 5) {
            json.keySet().mapNotNull(remote::getAuctionServer).forEach(::addItemsIfEnoughTimePassed)
        }.start()
    }

    override fun getItemStatistics(name: String, server: String): List<WoWAuctionItem>? {
        return itemRepo.getItemID(name)?.toString()?.let { id ->
            val jObj = json.getJSONObject(server)

            jObj.keySet()
                .filter { it.startsWith(id) }
                .map { Gson().fromJson("$jObj", WoWAuctionItem::class.java) }
        }
    }

    override fun lastModified(server: String) =
        json.getJSONObject(server).keySet().map { it.split("_")[0].toBigInteger() }.max()

    private fun addItemsIfEnoughTimePassed(server: WoWAuctionServer) {
        val remoteEpoch = server.lastUpdatedEpoch

        server.items
            .filter { remoteEpoch > it.modified }
            .forEach { write(server.name, "${it.id}_${it.modified}", Gson().toJson(it)) }
    }

}