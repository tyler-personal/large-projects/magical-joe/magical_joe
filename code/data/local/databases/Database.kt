package data.local.databases

import org.intellij.lang.annotations.Language
import java.sql.*


class Database(databaseName: String="magicaljoe") {
    private val databaseURL = "jdbc:mysql://localhost/$databaseName?autoReconnect=true&useSSL=false"
    private val user = "root"
    private val password = "root"

    fun executeSQL(@Language("SQL") sql: String) {
        executeWithStatement { it.execute(sql) }
    }

    fun <T> executeQuery(@Language("SQL") sql: String, nextBlock: (ResultSet) -> T): List<T> {
        return executeWithStatement { it.executeQuery(sql).mapAndClose(nextBlock) }
    }

    private fun <T> executeWithStatement(block: (Statement) -> T): T {
        val connection = DriverManager.getConnection(databaseURL, user, password)
        val statement = connection.createStatement()

        val t = block(statement)

        statement.close()
        connection.close()

        return t
    }

    private fun <T> ResultSet.mapAndClose(block: (ResultSet) -> T): List<T> {
        val li = mutableListOf<T>()
        while (this.next())
            li.add(block(this))
        this.close()
        return li
    }
}