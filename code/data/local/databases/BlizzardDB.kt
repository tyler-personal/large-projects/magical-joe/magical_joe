package data.local.databases

import domain.entities.PersistentThread
import domain.datastores.games.WoWAuctionDatastore
import domain.datastores.games.WoWItemDatastore
import domain.entities.structs.games.WoWAuctionItem
import domain.entities.structs.games.WoWAuctionServer
import lib.getBigInteger
import lib.epochToHours
import org.intellij.lang.annotations.Language
import java.math.BigInteger

class BlizzardDB(remote: WoWAuctionDatastore.Remote, private val items: WoWItemDatastore): WoWAuctionDatastore.Statistics {
    private val db = Database()

    init {
        PersistentThread(minutes = 5) {
            db.executeQuery("SELECT DISTINCT server from auctions") { it.getString("server") }
                .mapNotNull { remote.getAuctionServer(it) }
                .forEach(::addItemsIfEnoughTimePassed)
        }.start()
    }


    private fun getLastEpoch(server: String): Double? =
        db.executeQuery("SELECT DISTINCT modifiedEpoch FROM auctions WHERE server='$server'" ) {
            it.getDouble("modifiedEpoch")
        }.max()

    override fun lastModified(server: String): BigInteger? {
        return getLastEpoch(server)?.toBigDecimal()?.toBigInteger()
        // Actually I have a better idea than: return RepoContainer.wowAuctionRefresherRepo.lastModified(server)
    }

    override fun getItemStatistics(name: String, server: String): List<WoWAuctionItem>? {
        val id = items.getItemID(name)
        return if (id != null) {
            db.executeQuery("SELECT * FROM auctions WHERE itemID='$id' AND server='$server'") {
                val min = it.getBigInteger("min")
                val median25 = it.getBigInteger("25Median")
                val median10 = it.getBigInteger("10Median")
                val count = it.getInt("count")
                val modified = it.getBigInteger("modifiedEpoch")

                WoWAuctionItem(id, modified, count, min, median25, median10)
            }
        } else null
    }

    private fun addItemsIfEnoughTimePassed(server: WoWAuctionServer) {
        val modifiedEpochHour = server.lastUpdatedEpoch.toDouble().epochToHours()

        val lastEpochHour = getLastEpoch(server.name)?.epochToHours() ?: 0.0
        val distance = modifiedEpochHour - lastEpochHour

        if (distance > 0) {
            println("Last change: $distance - $${server.nameWithSpaces}")
            val values = server.items.joinToString(", ") { item ->
                "(${item.id}, ${item.minCostInt}, ${item.bottom25Int}, " +
                    "${item.bottom10Int}, ${item.count}, ${item.modified}, \"${server.name}\")"
            }
            @Language("SQL") val sql = """
                INSERT INTO auctions(itemID, min, 25Median, 10Median, count, modifiedEpoch, server)
                VALUES $values
            """.trimIndent()

            db.executeSQL(sql)
        }
    }
}