package data.remote

import domain.boundaries.datastores.BibleDatastore
import lib.safeURL
import org.json.JSONObject

class BibleAPI: BibleDatastore {
    override fun getVerse(book: String, chapter: Int, verse: Int): String? =
        gets(book, chapter, verse, verse).getString("text")

    override fun getVerses(book: String, chapter: Int, start: Int, end: Int): List<String>? =
         gets(book, chapter, start, end).getJSONArray("verses").map { (it as JSONObject).getString("text") }

    override fun getRandomVerse(): Pair<String, String> =
        khttp.get("http://labs.bible.org/api/?passage=random").text.split("</b>")
                .let { xs -> xs[0].removePrefix("<b>") to xs[1] }


    private fun gets(book: String, chapter: Int, start: Int, end: Int) =
        sequenceOf("", "1", "2", "3").mapNotNull { get("$it$book", chapter, start, end) }.first()

    private fun get(book: String, chapter: Int, start: Int, end: Int): JSONObject? {
        val result = khttp.get("https://bible-api.com/$book $chapter:$start-$end".safeURL()).jsonObject
        if (result.has("error"))
            return null
        return result
    }
}