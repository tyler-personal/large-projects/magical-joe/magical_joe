package data.repositories.api.floating

import domain.datastores.GenderDatastore
import java.io.File

class GenderAPI : GenderDatastore {
    private val apiKey = File("keys/gender.txt").readText()
    private val genderApi = "https://gender-api.com/get"
    private val genderCountryByCode = mapOf(
        "AD" to "Andorra",
        "AE" to "United Arab Emirates",
        "AF" to "Afghanistan",
        "AL" to "Albania",
        "AM" to "Armenia",
        "AO" to "Angola",
        "AR" to "Argentina",
        "AT" to "Austria",
        "AU" to "Australia",
        "AZ" to "Azerbaijan",
        "BA" to "Bosnia and Herzegovina",
        "BB" to "Barbados",
        "BD" to "Bangladesh",
        "BE" to "Belgium",
        "BF" to "Burkina Faso",
        "BG" to "Bulgaria",
        "BH" to "Bahrain",
        "BI" to "Burundi",
        "BJ" to "Benin",
        "BN" to "Brunei",
        "BO" to "Bolivia",
        "BR" to "Brazil",
        "BS" to "Bahamas",
        "BW" to "Botswana",
        "BY" to "Belarus",
        "CA" to "Canada",
        "CD" to "Congo - Kinshasa",
        "CG" to "Congo - Brazzaville",
        "CH" to "Switzerland",
        "CI" to "Côte d’Ivoire",
        "CL" to "Chile",
        "CM" to "Cameroon",
        "CN" to "China",
        "CO" to "Colombia",
        "CR" to "Costa Rica",
        "CU" to "Cuba",
        "CV" to "Cape Verde",
        "CY" to "Cyprus",
        "CZ" to "Czech Republic",
        "DE" to "Germany",
        "DJ" to "Djibouti",
        "DK" to "Denmark",
        "DO" to "Dominican Republic",
        "DZ" to "Algeria",
        "EC" to "Ecuador",
        "EE" to "Estonia",
        "EG" to "Egypt",
        "ES" to "Spain",
        "ET" to "Ethiopia",
        "FI" to "Finland",
        "FJ" to "Fiji",
        "FR" to "France",
        "GA" to "Gabon",
        "GB" to "United Kingdom",
        "GE" to "Georgia",
        "GF" to "French Guiana",
        "GH" to "Ghana",
        "GI" to "Gibraltar",
        "GM" to "Gambia",
        "GN" to "Guinea",
        "GP" to "Guadeloupe",
        "GQ" to "Equatorial Guinea",
        "GR" to "Greece",
        "GT" to "Guatemala",
        "GY" to "Guyana",
        "HK" to "Hong Kong SAR China",
        "HN" to "Honduras",
        "HR" to "Croatia",
        "HT" to "Haiti",
        "HU" to "Hungary",
        "ID" to "Indonesia",
        "IE" to "Ireland",
        "IL" to "Israel",
        "IN" to "India",
        "IQ" to "Iraq",
        "IR" to "Iran",
        "IS" to "Iceland",
        "IT" to "Italy",
        "JE" to "Jersey",
        "JM" to "Jamaica",
        "JO" to "Jordan",
        "JP" to "Japan",
        "KE" to "Kenya",
        "KG" to "Kyrgyzstan",
        "KH" to "Cambodia",
        "KM" to "Comoros",
        "KR" to "South Korea",
        "KW" to "Kuwait",
        "KZ" to "Kazakhstan",
        "LA" to "Laos",
        "LB" to "Lebanon",
        "LK" to "Sri Lanka",
        "LR" to "Liberia",
        "LS" to "Lesotho",
        "LT" to "Lithuania",
        "LU" to "Luxembourg",
        "LV" to "Latvia",
        "LY" to "Libya",
        "MA" to "Morocco",
        "MC" to "Monaco",
        "MD" to "Moldova",
        "ME" to "Montenegro",
        "MG" to "Madagascar",
        "MK" to "Macedonia",
        "ML" to "Mali",
        "MM" to "Myanmar [Burma]",
        "MN" to "Mongolia",
        "MQ" to "Martinique",
        "MR" to "Mauritania",
        "MT" to "Malta",
        "MU" to "Mauritius",
        "MV" to "Maldives",
        "MW" to "Malawi",
        "MX" to "Mexico",
        "MY" to "Malaysia",
        "MZ" to "Mozambique",
        "NA" to "Namibia",
        "NC" to "New Caledonia",
        "NE" to "Niger",
        "NG" to "Nigeria",
        "NI" to "Nicaragua",
        "NL" to "Netherlands",
        "NO" to "Norway",
        "NP" to "Nepal",
        "NZ" to "New Zealand",
        "OM" to "Oman",
        "PA" to "Panama",
        "PE" to "Peru",
        "PF" to "French Polynesia",
        "PG" to "Papua New Guinea",
        "PH" to "Philippines",
        "PK" to "Pakistan",
        "PL" to "Poland",
        "PR" to "Puerto Rico",
        "PS" to "Palestinian Territories",
        "PT" to "Portugal",
        "PY" to "Paraguay",
        "QA" to "Qatar",
        "RE" to "Réunion",
        "RO" to "Romania",
        "RS" to "Serbia",
        "RU" to "Russia",
        "RW" to "Rwanda",
        "SA" to "Saudi Arabia",
        "SC" to "Seychelles",
        "SD" to "Sudan",
        "SE" to "Sweden",
        "SG" to "Singapore",
        "SH" to "Saint Helena",
        "SI" to "Slovenia",
        "SK" to "Slovakia",
        "SL" to "Sierra Leone",
        "SN" to "Senegal",
        "SO" to "Somalia",
        "SV" to "El Salvador",
        "SY" to "Syria",
        "SZ" to "Swaziland",
        "TD" to "Chad",
        "TG" to "Togo",
        "TH" to "Thailand",
        "TJ" to "Tajikistan",
        "TM" to "Turkmenistan",
        "TN" to "Tunisia",
        "TR" to "Turkey",
        "TT" to "Trinidad and Tobago",
        "TW" to "Taiwan",
        "TZ" to "Tanzania",
        "UA" to "Ukraine",
        "UG" to "Uganda",
        "US" to "United States",
        "UY" to "Uruguay",
        "UZ" to "Uzbekistan",
        "VE" to "Venezuela",
        "VN" to "Vietnam",
        "YE" to "Yemen",
        "ZA" to "South Africa",
        "ZM" to "Zambia",
        "ZW" to "Zimbabwe"
    )

    override val countryStrings: List<String> = getCountryStrings()
    override fun get(name: String, countryCode: String?): String {
        return countryCode?.let {
            getGender(name, it)
        } ?: getGender(name)
    }

    private fun getCountryStrings(countriesPerString: Int=100): List<String> {
        return genderCountryByCode.asSequence().chunked(countriesPerString).map { message ->
                message.fold("") { acc, x -> acc + "${x.key} to ${x.value}\n" }
        }.toList()
    }

    private fun getGender(name: String): String {
        val result = khttp.get("$genderApi?name=$name&key=$apiKey").jsonObject
        return "Hello $name. I'm ${result["accuracy"]}% sure you're a ${result["gender"]}"
    }

    private fun getGender(name: String, countryCode: String): String {
        return if (genderCountryByCode.containsKey(countryCode)) {
            val country = genderCountryByCode[countryCode]
            val result = khttp.get("$genderApi?name=$name&country=$countryCode&key=$apiKey").jsonObject

            if (result.has("errmsg"))
                "Sorry the following error occurred: ${result["errmsg"]}"
            else
                "Hello $name.\n" +
                    "Since you're from $country, I'm ${result["accuracy"]}% sure you're a ${result["gender"]}"
        }
        else
            "That is not a valid country code. Type !nameCountries to see valid countries."
    }
}