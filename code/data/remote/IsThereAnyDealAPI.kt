package data.remote

import domain.datastores.games.SteamDatastore
import domain.entities.structs.SimpleSteamApp
import domain.entities.structs.SteamApp
import khttp.get
import lib.tryOrNull
import log
import org.json.JSONObject
import java.io.File

class IsThereAnyDealAPI: SteamDatastore {
    private val base = "https://api.isthereanydeal.com"
    private val v1 = "$base/v01"
    private val v2 = "$base/v02"
    private val key = File("keys/anyDeal.txt").readText()
    private val region = "us"

    private var namesByID = mapOf<Int, String>()

    init {
        while(namesByID.isEmpty()) {
            try {
                namesByID = khttp.get("http://api.steampowered.com/ISteamApps/GetAppList/v0002/")
                    .jsonObject.getJSONObject("applist").getJSONArray("apps")
                    .map { JSONObject(it.toString()) }
                    .map { it.getInt("appid") to it.getString("name") }
                    .toMap()
            } catch(e: Exception) { log("SteamAPI: Failed") }
        }
        log("${namesByID.size} items in IsThereAnyDealAPI (for steam)")
    }

    override fun getSteamApp(name: String, id: Int): SteamApp? {
        val plain = getPlain("$id") ?: return null
        val lowestSteam = tryOrNull { getOneApp("game/lowest", plain, listOf("steam")).getDouble("price") } ?: return null
        // val current = getOneApp("game/price", id).getInt("price")
        return SteamApp(id, name, lowestSteam, getPrice(id))
    }

    override fun getAllSteamApps(): List<SimpleSteamApp> {
        return namesByID.map { (id, name) ->
            SimpleSteamApp(name, id) { getSteamApp(name, id) }
        }
    }


    private fun getPrice(id: Int): Double {
        val data = khttp.get("http://store.steampowered.com/api/appdetails?appids=$id")
            .jsonObject.getJSONObject("$id").getJSONObject("data")
        val price = data.getJSONObject("price_overview")
        return price.getInt("final") / 100.0
    }

    override fun getNameByID(appID: Int): String = namesByID[appID] ?: "N.A"

    private fun getOneApp(part: String, plain: String, shops: List<String> = listOf()) =
        getApps(part, listOf(plain), shops).getJSONObject(plain)

    private fun getApps(part: String, plains: List<String>, shops: List<String> = listOf()): JSONObject {
        val shop = if (shops.isEmpty()) "" else "&shops=${shops.joinToString(",")}"
        return get("$v1/$part?key=$key&plains=${plains.joinToString(",")}&region=$region$shop")
            .jsonObject.getJSONObject("data")
    }

    private fun getPlain(id: String) = tryOrNull { getPlains(listOf(id)).getString("app/$id") }

    private fun getPlains(ids: List<String>) =
        get("$v1/game/plain/id/?key=$key&shop=steam&ids=${ids.joinToString(",") { "app/$it" }}")
            .jsonObject.getJSONObject("data")
}