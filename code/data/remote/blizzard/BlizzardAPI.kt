package data.remote.blizzard

import khttp.get
import khttp.post
import khttp.structures.authorization.BasicAuthorization
import log
import java.io.File

abstract class BlizzardAPI {
    private val base = "https://us.api.blizzard.com/wow"
    private val clientID = File("keys/blizzard/clientID.txt").readText()
    private val clientSecret = File("keys/blizzard/clientSecret.txt").readText()
    private val token = post(
        "https://us.battle.net/oauth/token",
        auth=BasicAuthorization(clientID, clientSecret),
        data=mapOf("grant_type" to "client_credentials")
    ).jsonObject.opt("access_token") ?: "true" // TODO: Fix integration
    internal fun request(url: String) = get("$base/$url${if (url.contains("?")) "&" else "?"}access_token=$token")
}