package data.remote.blizzard

import data.local.memory.WoWItemMemoryStore
import domain.datastores.games.WoWItemDatastore
import domain.interactors.XMLRetriever
import lib.tryOrNull

class WoWItemAPI: BlizzardAPI(), WoWItemDatastore {
    private val memStore = WoWItemMemoryStore()
    override fun getItemName(id: Int): String? =
        memStore.getName(id) ?: request("item/$id").jsonObject.optString("name")

    override fun getItemID(name: String): Int? {
        val item = name.replace(" ", "%20")
        val xml = XMLRetriever.perform("https://www.wowhead.com/item=$item&xml")
        val attributes = tryOrNull { xml.getElementsByTagName("item").item(0).attributes } ?: return null

        return attributes.getNamedItem("id").toString().removePrefix("id=\"").removeSuffix("\"").toInt()
    }
}