package data.remote.blizzard

import data.local.databases.BlizzardDB
import domain.datastores.games.WoWAuctionDatastore
import domain.entities.structs.games.WoWAuctionItem
import domain.entities.structs.games.WoWAuctionServer
import khttp.get
import org.json.*
import lib.safeURL
import lib.toBigInteger
import java.math.*

class WoWAuctionAPI: BlizzardAPI(), WoWAuctionDatastore.Remote {
    private fun getInitialAuctionData(server: String): InitialAuctionData? {
        val result = request("auction/data/${server.safeURL()}").jsonObject.optJSONArray("files")?.getJSONObject(0)
            ?: return null
        val url = result.optString("url") ?: return null
        val lastModifiedEpoch = result.getBigInteger("lastModified") ?: return null
        return InitialAuctionData(url, lastModifiedEpoch)
    }

    override fun lastModified(server: String): BigInteger? = getInitialAuctionData(server)?.lastModifiedEpoch

    override fun getAuctionServer(server: String): WoWAuctionServer? {
        val initialAuctionData = getInitialAuctionData(server) ?: return null
        val modified = initialAuctionData.lastModifiedEpoch
        val auctions = get(initialAuctionData.url).jsonObject.optJSONArray("auctions") ?: return null

        val pricesByID = auctions.map { it as JSONObject }.mapNotNull { auction ->
            val itemID = auction.optInt("item") ?: return null
            val buyout = auction.getBigInteger("buyout") ?: return null
            val quantity = auction.optInt("quantity") ?: return null

            val buyoutPer = buyout.toDouble() / quantity.toDouble()
            if (buyoutPer > 0.0)
                itemID to buyoutPer
            else
                null
        }.groupBy { it.first }.map { map ->
            map.key to map.value.map { it.second } }
        .toMap()

        val items = pricesByID.map {
            val sortedPrices = it.value.sorted()
            WoWAuctionItem(
                id=it.key,
                modified=modified,
                count=it.value.count(),
                minCostInt=sortedPrices[0].toBigInteger(),
                bottom25Int=sortedPrices.getOrNull(sortedPrices.size/4)?.toBigInteger(),
                bottom10Int=sortedPrices.getOrNull(sortedPrices.size/10)?.toBigInteger()
            )
        }

        return WoWAuctionServer(server, modified, items)
    }

    private class InitialAuctionData(val url: String, val lastModifiedEpoch: BigInteger)
}