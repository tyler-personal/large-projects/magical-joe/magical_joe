package data.remote.blizzard

import domain.datastores.games.WoWCharacterDatastore
import domain.entities.structs.games.WoWCharacter


class WoWCharacterAPI: BlizzardAPI(), WoWCharacterDatastore {
    override fun get(name: String, server: String): WoWCharacter? {
        val response = request("character/$server/$name?fields=items").jsonObject
        val items = response.optJSONObject("items") ?: return null

        val itemLevel = items.getInt("averageItemLevel") ?: return null
        // val equippedItemLevel = items.getInt("averageItemLevelEquipped")
        return WoWCharacter(name, itemLevel)
    }
}