package data.remote.albion

import domain.datastores.UserDatastore
import domain.datastores.games.AlbionDatastore
import domain.entities.structs.games.albion.AlbionItem
import domain.entities.structs.games.albion.AlbionLocation
import domain.interactors.*
import domain.views.users.User
import khttp.*
import lib.*
import log


class AlbionAPI(private val userRepo: UserDatastore? = null): AlbionDatastore {
    private data class ItemFromJSON(val id: String, val name: String, val description: String?)
    // TODO: Replace with local json
    private val itemsByName = get(
        "https://raw.githubusercontent.com/broderickhyman/ao-bin-dumps/master/formatted/items.json"
    ).jsonArray.jsonObjects.mapNotNull { jObj ->
        fun getEnglishValue(arrName: String) =
            jObj.optJSONArray(arrName)?.jsonObjects?.find { it["Key"] == "EN-US" }?.getString("Value")
        val id = jObj.getString("LocalizationNameVariable").removePrefix("@ITEMS_")
        val description = getEnglishValue("LocalizedDescriptions")
        val name = getEnglishValue("LocalizedNames") ?: id.split("_").from(3)
            ?.map { it.toLowerCase().titleize() }?.join(" ")

        name?.let { it to ItemFromJSON(id, it, description) }
    }.toMap()

    private val names = itemsByName.keys

    override fun getItem(name: String, user: User?): AlbionItem? {
        val closestName = StringSearcher.perform(name, names, userRepo, user)
        val item = itemsByName[closestName] ?: return null
        return AlbionItem(closestName, item.description, getItems(item.id))
    }

    private fun getItems(id: String): Map<AlbionLocation, AlbionItem.Price> =
        get("https://www.albion-online-data.com/api/v2/stats/prices/$id")
            .jsonArray.jsonObjects
            .map { jObj ->
                val city = AlbionLocation.valueOf(jObj.getString("city").toUpperCase().replace(" ", "_"))
                val price = AlbionItem.Price(
                    jObj.getInt("sell_price_min"),
                    jObj.getInt("buy_price_min"),
                    jObj.getInt("sell_price_max"),
                    jObj.getInt("buy_price_max")
                )
                city to price
            }.toMap()


}