package data.repositories.api.floating

import khttp.get

object YandexAPI {
    private const val url = "https://translate.yandex.net/api/v1.5/tr.json/translate"
    private const val key = "trnsl.1.1.20180823T061548Z.b62e21e00bfdeafe.2be70f48ce5f3eb1e1b6691c9c4760e49e0547de"

    fun translate(unsafeText: String, fromLanguage: String, toLanguage: String): String {
        val text = unsafeText.replace(" ", "%20")
        val jObj = get("$url?key=$key&text=$text&lang=$fromLanguage-$toLanguage").jsonObject
        return when(jObj.getInt("code")) {
            200 -> jObj.getJSONArray("text").getString(0)
            501 -> jObj.getString("message")
            502 -> jObj.getString("message")
            else -> "Unknown Error Occurred. Verify that your languages are correct."
        }
    }
}