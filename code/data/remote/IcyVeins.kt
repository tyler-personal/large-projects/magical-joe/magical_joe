package data.remote

import domain.datastores.games.HeroesDatastore
import domain.entities.structs.games.heroes.*
import domain.interactors.*
import khttp.get
import lib.*
import org.json.JSONObject

// TODO: Extract the guides out as a job so we don't spam scrape their site.
class IcyVeins : HeroesDatastore {
    private val json = get("http://hotsapi.net/api/v1/heroes").jsonArray.jsonObjects
    override val heroesNames: List<String> get() = json.map { it.getString("name") }

    private fun getHeroJSON(name: String): JSONObject? = json.find { it.getString("name") == name }

    override fun getHeroInfo(inputName: String): Hero {
        // val safeName = name.replace("." to "-", "'" to "", " " to "")
        val safeName = when (inputName) {
            "D.Va" -> "dva"
            else -> inputName.replace(".", "-").replace("'", "").replace(" ", "-").replace("ú", "u").toLowerCase()
        }
        val realName = StringSearcher.perform(inputName, heroesNames)

        val xml = HTMLRetriever.perform("https://www.icy-veins.com/heroes/$safeName-build-guide".replace("--", "-"))
        val (synergies, counters) = listOf("synergies", "counters").map { type ->
            xml.select(".heroes_$type .heroes_synergies_counters_content div div a")
                .map { it.attr("href").between("heroes/", "-build").titleize() }
        }

        val role = HeroesRole.valueOf(getHeroJSON(realName)!!.getString("role"))
        return Hero(realName, role, synergies, counters, listOf(), this)
    }
}

fun main() {
    val json = get("http://hotsapi.net/api/v1/heroes").jsonArray.jsonObjects.map { it.getString("role") }.toSet()
    println(json)
    IcyVeins().getHeroInfo("E.T.C").let { println(it.counterNames) }

}