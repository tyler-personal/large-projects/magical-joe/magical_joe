package data.remote.google

import domain.datastores.ImageDatastore
import domain.entities.structs.ImageAnnotations
import lib.tryOrNull
import org.json.*
import java.util.*
import log

class GoogleImageAnnotator: ImageDatastore {
    override val name = "Google Vision API"
    override fun generateFromBytes(bytes: ByteArray): ImageAnnotations? {
        val byteString = Base64.getEncoder().encodeToString(bytes)
        val key = "AIzaSyCg5D9SlrQJuur72H9411YdhSdVP-dbsmk"
        val (response, web) = tryOrNull {
            khttp.post("https://vision.googleapis.com/v1/images:annotate?key=$key", data="""
                {
                  "requests":[{
                      "image":{"content":"$byteString"},
                      "features":[
                        {"type":"LABEL_DETECTION", "maxResults":3},
                        {"type":"WEB_DETECTION", "maxResults": 3}
                      ]
                  }]
                }
            """.trimIndent()).jsonObject.getJSONArray("responses").getJSONObject(0)
                    .let { Pair(it, it.getJSONObject("webDetection")) }
        } ?: return null

        return ImageAnnotations(
            labels = getResults(response.optJSONArray("labelAnnotations")),
            web = getResults(web?.optJSONArray("webEntities")),
            urlsWithImage = web?.optJSONArray("pagesWithMatchingImages")?.mapNotNull {
                (it as JSONObject).getString("url")
            },
            similarImageURLs = web?.optJSONArray("visuallySimilarImages")?.mapNotNull {
                (it as JSONObject).getString("url")
            }

        )
    }

    private fun getResults(li: JSONArray?): List<ImageAnnotations.Result>? = li?.map { it as JSONObject }
        ?.map { ImageAnnotations.Result( it.optString("description"), it.optDouble("score") ) }

    private fun getURLs(web: JSONObject?, str: String) = web?.optJSONArray(str)?.mapNotNull {
        (it as JSONObject).getString("url")
    }
}