package data.repositories.api.google

import domain.boundaries.datastores.images.ChartDatastore
import lib.safeURL

class GoogleChartMaker: ChartDatastore {
    override fun make(name: String, values: List<List<Int>>, colors: List<String>): String {
        values.forEach { println(it) }
        val min = values.mapNotNull { it.min() }.min()!!
        val max = values.mapNotNull { it.max() }.max()!!

        val newValues = values.joinToString("|") { li ->
            li.asSequence().map { transition(it, min, max) }.joinToString(",")
        }

        return "https://chart.googleapis.com/chart" +
            "?cht=lc" +
            "&chd=t:$newValues" +
            "&chs=850x350" +
            "&chtt=${name.safeURL()}" +
            "&chts=FFFFFF,24,c" +
            "&chxt=y" +
            "&chxr=0,$min,$max" +
            "&chf=bg,s,000000" +
            "&chco=${colors.joinToString(",")}"
    }

    private fun transition(num: Int, min: Int, max: Int, newMin: Int=0, newMax: Int=100): Int {
        if (max-min == 0)
            return 1

        return (((newMax - newMin) * (num - min)) / (max - min)) + newMin
    }
}