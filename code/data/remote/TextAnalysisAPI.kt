package data.remote

import com.aylien.textapi.TextAPIClient
import com.aylien.textapi.parameters.*
import domain.boundaries.datastores.TextDatastore

class TextAnalysisAPI : TextDatastore {
    private val client = TextAPIClient("23a51beb", "22a48e986b8cb3213f2c591cb0e4e48f")

    override fun getKeywords(text: String): List<String> {
        val params = EntitiesParams.newBuilder().apply {
            setText(text)
        }.build()
        val concepts = client.entities(params)
        return concepts.entities.firstOrNull { it.type == "keyword" }?.surfaceForms?.toList() ?: listOf()
    }

    fun getKeywordsString(text: String): String =
        "The entities in your text are: " + getKeywords(text).reduce { x, y -> "$x, $y" }
}