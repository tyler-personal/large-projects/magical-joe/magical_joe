package data.remote

import khttp.get


object RecipiesAPI {
    private const val baseURL = "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes"

    fun getByIngredients(ingredientsList: List<String>) {
        val ingredients = ingredientsList.reduce { acc, x -> "$acc%2$x" }
        val response = get("$baseURL/findByIngredients?ingredients=$ingredients&number=5&ranking=1")
    }

}