package data.remote

import domain.datastores.ActorDatastore
import domain.entities.structs.movies.*
import khttp.get
import lib.jsonObjects
import lib.safeURL
import org.json.JSONObject

class TheMovieDB : ActorDatastore {
    private val base = "https://api.themoviedb.org/3"
    private val key = "api_key=8cb3d4b17eeff69515b5da3bbf2e1804"
    private val lang = "language=en-US"

    private fun fetch(url: String, extraParams: String = "") =
        get("$base/$url?&$key&$lang$extraParams".safeURL()).jsonObject

    override fun findMovieByID(movie: Int): String? =
        fetch("movie/$movie").optString("original_title")
    override fun findActorByID(actor: Int): String? =
        fetch("person/$actor").optString("name")

    private fun findMovieByName(name: String): JSONObject? =
        fetch("search/movie", "&query=$name&page=1&include_adult=false")
            .getJSONArray("results").optJSONObject(0)
    private fun findActorByName(name: String): JSONObject? =
        fetch("search/person", "&query=$name&page=1&include_adult=false")
            .getJSONArray("results").optJSONObject(0)

    private fun getMovieActorIDs(movie: Int) =
        fetch("movie/$movie/credits")
            .getJSONArray("cast").jsonObjects.map { it.getInt("id") }
    private fun getActorMovieIDs(person: Int) =
        fetch("person/$person/movie_credits")
            .getJSONArray("cast").jsonObjects.map { it.getInt("id") }


    override fun findMovie(movie: MovieQuery): MovieResult? {
        val movieJSON = findMovieByName(movie.name) ?: return null
        val id = movieJSON.optInt("id") ?: return null
        val actorIDs = getMovieActorIDs(id)

        return MovieResult(movieJSON.getString("original_title"), id, movie.name, actorIDs)
    }
    override fun findActor(actor: ActorQuery): ActorResult? {
        val actorJSON = findActorByName(actor.name) ?: return null
        val id = actorJSON.optInt("id") ?: return null
        val movieIDs = getActorMovieIDs(id)

        return ActorResult(actorJSON.getString("name"), id, actor.name, movieIDs)
    }
}